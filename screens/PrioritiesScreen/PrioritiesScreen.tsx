import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator
} from "react-native";

import { Text, TextInput } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useFocusEffect } from '@react-navigation/native';

import TopNavigator from "screens/TopNavigator/TopNavigator";

import styles from "./PrioritiesScreenStyles.styles";

import moment from 'moment';
import axios from "services/api";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AppContext } from "../../context/AppContext";

const PrioritiesScreen = ({ navigation }) => {
    const [id, setId] = React.useState(0);
    const [priorities, setPriorities] = React.useState("weight loss");
    const [typeOfTraining, setTypeOfTraining] = React.useState("endurance");
    const [prePostTraining, setPrePostTraining] = React.useState("pre workout");
    const [currentWeight, setCurrentWeight] = React.useState(0);
    const [switchTrigger, setSwitchTrigger] = React.useState(false);
    const contextData = React.useContext(AppContext);

    const [modalVisibility, setModalVisibility] = React.useState(false);
    const [submitDisabled, setSubmitDisabled] = React.useState(false);

    const [filteredItems, setFilteredItems] = React.useState([]);
    const [filteredDataLoading, setFilteredDataLoading] = React.useState(false);

    const [screenFocused, setScreenFocused] = React.useState(false);

    const atheleteProfileData = {
        user_id: id,
        priorities: priorities,
        type_of_training: typeOfTraining
    };

    const weightData = {
        user_id: id,
        current_weight: currentWeight,
    };

    React.useEffect(() => {
        saveAthleteProfileData();
    }, [switchTrigger]);

    useFocusEffect(
        React.useCallback(() => {
            if(screenFocused === false) {
                userAsyncDataFetch();
                filteredDataFetch("pre workout", priorities, typeOfTraining);
            }
            return () => {
                setScreenFocused(true);
            };
        }, [screenFocused])
    );

    let saveAthleteProfileData = async () => {
        await axios(contextData.userAccessTokenContext)
            .post("/user/athlete-profile/update", atheleteProfileData)
            .then(async(res) => {
                saveAsyncAndContextData(res.data.user);
            }).catch((err) => {
               console.log(err);
            });
    };

    let saveWeightData = async () => {
        setSubmitDisabled(true);
        setModalVisibility(false);
        await axios(contextData.userAccessTokenContext)
            .post("/user/weight/update", weightData)
            .then(async(res) => {
                saveAsyncAndContextData(res.data.user);
                setSubmitDisabled(false);
            }).catch((err) => {
               console.log(err);
            });
    };

    let saveAsyncAndContextData = async (user_data) => {
        await AsyncStorage.removeItem("user");
        const jsonValue = JSON.stringify(user_data);
        contextData.setUserDataContext(user_data);
        await AsyncStorage.setItem("user", jsonValue);
    };

    let userAsyncDataFetch = async () => {
        let userParsedData = await AsyncStorage.getItem("user");
        let user_data = JSON.parse(userParsedData);
        setId(user_data.id);
        setPriorities((user_data.athlete_profile?.priorities == null) ? "weight loss" : user_data.athlete_profile.priorities);
        setTypeOfTraining((user_data.athlete_profile?.type_of_training == null) ? "endurance" : user_data.athlete_profile.type_of_training);

        if (moment(user_data.weight?.created_at).format('LL') == moment(new Date).format('LL')) { 
            setCurrentWeight(user_data.weight?.weight);
        } else { 
            setCurrentWeight(user_data.weight?.weight);
        }
    }

    let filteredDataFetch = async (prePostTrainingFilter, prioritiesFilter, typeOfTrainingFilter) => {
        setFilteredDataLoading(true);
        let types = [prePostTrainingFilter];
        let keywords = [prioritiesFilter, typeOfTrainingFilter];
        await axios(contextData.userAccessTokenContext)
            .post(`/products/filter`, { types: types, keywords: keywords })
            .then(async(res) => {
                setFilteredItems(res.data.filtered_product_list);
                setFilteredDataLoading(false);
            }).catch((err) => {
                setFilteredDataLoading(false);
            });
    };

    const productDetailScreen = (item) => {
        navigation.navigate("ProductDetailScreen", item);
    };

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
            <SafeAreaView>
                <ScrollView>
                    <View style={styles.mainContainer}>
                        <TopNavigator refresh={true} subpage="false" />
                        <View style={styles.cardNoBg}>
                            <Text style={styles.cardHeadline}>My Weight</Text>
                            <View style={[styles.flxDrtnRow, styles.marginBottom5, styles.alignItemsCenter]}>
                                <View style={[styles.width50Percent, styles.flxDrtnRow, styles.alignItemsCenter]}>
                                    <Text style={[styles.blackText]}>
                                        Current Weight in kg:
                                    </Text>
                                    <TouchableOpacity style={styles.weightBtn} onPress={() => setModalVisibility(true)}>
                                        <Text style={[styles.blackText, styles.textBold]}>{currentWeight}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text style={styles.cardHeadline}>Your Priorities</Text>
                            <View style={[styles.flxDrtnRow, styles.marginBottom5, styles.alignItemsCenter]}>
                                <TouchableOpacity style={[styles.categoryTabs, (priorities == "weight loss") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setPriorities("weight loss"); setSwitchTrigger(!switchTrigger);  filteredDataFetch(prePostTraining, "weight loss", typeOfTraining); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (priorities == "weight loss") ? styles.limeBoldCenterText : styles.blackCenterText]}>Weight Loss</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.categoryTabs, styles.marginLeft5, (priorities == "overall fitness") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setPriorities("overall fitness"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, "overall fitness", typeOfTraining); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (priorities == "overall fitness") ? styles.limeBoldCenterText : styles.blackCenterText]}>Overall Fitness</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.categoryTabs, styles.marginLeft5, (priorities == "muscle gain") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setPriorities("muscle gain"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, "muscle gain", typeOfTraining); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (priorities == "muscle gain") ? styles.limeBoldCenterText : styles.blackCenterText]}>Muscle Gain</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.flxDrtnRow, styles.marginBottom5, styles.alignItemsCenter]}>
                                <TouchableOpacity style={[styles.categoryTabs, (priorities == "performance boost") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setPriorities("performance boost"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, "performance boost", typeOfTraining); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (priorities == "performance boost") ? styles.limeBoldCenterText : styles.blackCenterText]}>Performance Boost</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.categoryTabs, styles.marginLeft5, (priorities == "recovery") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setPriorities("recovery"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, "recovery", typeOfTraining); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (priorities == "recovery") ? styles.limeBoldCenterText : styles.blackCenterText]}>Recovery</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.categoryTabs, styles.marginLeft5, (priorities == "flexibility") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setPriorities("flexibility"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, "flexibility", typeOfTraining); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (priorities == "flexibility") ? styles.limeBoldCenterText : styles.blackCenterText]}>Flexibility</Text>
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.cardHeadline}>Type of Training</Text>
                            <View style={[styles.flxDrtnRow, styles.marginBottom5, styles.alignItemsCenter]}>
                                <TouchableOpacity style={[styles.categoryTabs, (typeOfTraining == "endurance") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setTypeOfTraining("endurance"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, priorities, "endurance"); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (typeOfTraining == "endurance") ? styles.limeBoldCenterText : styles.blackCenterText]}>Endurance</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.categoryTabs, styles.marginLeft5, (typeOfTraining == "hypothropy") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setTypeOfTraining("hypothropy"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, priorities, "hypothropy"); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (typeOfTraining == "hypothropy") ? styles.limeBoldCenterText : styles.blackCenterText]}>Hypothropy</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.categoryTabs, styles.marginLeft5, (typeOfTraining == "hiit") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setTypeOfTraining("hiit"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, priorities, "hiit"); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (typeOfTraining == "hiit") ? styles.limeBoldCenterText : styles.blackCenterText]}>HIIT</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.flxDrtnRow, styles.marginBottom5, styles.alignItemsCenter]}>
                                <TouchableOpacity style={[styles.categoryTabs, (typeOfTraining == "cardio") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setTypeOfTraining("cardio"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, priorities, "cardio"); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (typeOfTraining == "cardio") ? styles.limeBoldCenterText : styles.blackCenterText]}>Cardio</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.categoryTabs, styles.marginLeft5, (typeOfTraining == "weight") ? styles.bdrBgLimeCustom : styles.borderGrayCustom]} onPress={() => { setTypeOfTraining("weight"); setSwitchTrigger(!switchTrigger); filteredDataFetch(prePostTraining, priorities, "weight"); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (typeOfTraining == "weight") ? styles.limeBoldCenterText : styles.blackCenterText]}>Weight</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.switchContainer}>
                                <TouchableOpacity style={[styles.switchSelection, (prePostTraining == "pre workout") ? styles.bgBlack : styles.bgTransparent]} onPress={() => { setPrePostTraining("pre workout"); filteredDataFetch("pre workout", priorities, typeOfTraining); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (prePostTraining == "pre workout") ? styles.limeBoldText : styles.blackText]}>Pre Training</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.switchSelection, styles.marginLeft15, (prePostTraining == "post workout") ? styles.bgBlack : styles.bgTransparent]} onPress={() => { setPrePostTraining("post workout"); filteredDataFetch("post workout", priorities, typeOfTraining); }}>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (prePostTraining == "post workout") ? styles.limeBoldText : styles.blackText]}>Post Training</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.card, styles.marginBottom20]}>
                                <View style={[styles.flxDrtnRow, styles.justifyContentSpaceBetween]}>
                                    <Text style={styles.cardHeadline}>Recommendations</Text>
                                    <Text style={styles.cardHeadline}>
                                        {/* <TouchableOpacity style={styles.favoritesBtn}>
                                            <MaterialCommunityIcons
                                                style={{ marginRight: 2 }}
                                                name={"heart"}
                                                color={"#C9EA3B"}
                                                size={14}
                                            /> 
                                            <Text>Favorites</Text>
                                        </TouchableOpacity> */}
                                    </Text>
                                </View>
                                {filteredItems.length > 0 && filteredDataLoading == false && (
                                    <View style={[styles.flxDrtnRow, styles.flxWrap]}>
                                    {
                                        filteredItems.map((item, index) => {
                                            return (
                                                <View key={"flr"+item.id} style={styles.productContainer}>
                                                    <TouchableOpacity onPress={() => productDetailScreen(item)}>
                                                        <View style={styles.productInfo}>
                                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.textBold, styles.marginRight5]}>{item.price_in_points}</Text>
                                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.textBold, styles.fontSize11]}>{item.price_in_aud} AUD</Text>
                                                        </View>
                                                        <Image source={(item.media.length > 0) ? { uri: item.media[0].path } : require("../../assets/placeholder-supplement.jpg")} style={styles.productImage}/>
                                                        <Text style={styles.productName}>
                                                            {item.name}
                                                        </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            );
                                        })
                                    }
                                    </View>
                                )}
                                {filteredDataLoading && (
                                    <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                                )}
                                {filteredItems.length <= 0 && filteredDataLoading == false && (
                                    <Text>No available recommendation.</Text>
                                )}
                            </View>
                        </View>
                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={modalVisibility}
                            onRequestClose={() => {
                                setModalVisibility({ modalVisible: false });
                            }}
                        >
                            <View style={[styles.flex1, styles.alignItemsCenter, styles.justifyContentCenter]}>
                                <View style={[styles.card, styles.marginVertical0, styles.width80Percent]}>
                                    <Text style={[styles.cardHeadline, styles.marginBottom10]}>Enter current weight (kg)</Text>
                                    <TextInput
                                        style={styles.textInput}
                                        underlineColor="transparent"
                                        activeUnderlineColor="transparent"
                                        value={currentWeight}
                                        keyboardType="number-pad"
                                        onChangeText={(text) => setCurrentWeight(text)}
                                    />
                                    <View style={[styles.flxDrtnRow, styles.marginTop10]}>
                                        <TouchableOpacity style={styles.submitBtn} disabled={submitDisabled} onPress={() => { saveWeightData() }}>
                                            <Text style={styles.limeBoldText}>SUBMIT</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.closeBtn} onPress={() => { setModalVisibility(false); }}>
                                            <Text style={styles.blackText}>CLOSE</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default PrioritiesScreen;