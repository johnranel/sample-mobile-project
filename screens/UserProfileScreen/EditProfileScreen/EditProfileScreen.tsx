import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity,
    Image,
    Platform
} from "react-native";

import { TextInput, Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import DateTimePicker from '@react-native-community/datetimepicker';
import * as ImagePicker from 'expo-image-picker';
import { useFocusEffect } from '@react-navigation/native';
import AnimatedLoader from "react-native-animated-loader";

import styles from "./EditProfileScreenStyles.styles";

import moment from 'moment';
import axios from "services/api";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AppContext } from "../../../context/AppContext";
import TopNavigator from "screens/TopNavigator/TopNavigator";

const EditProfileScreen = ({ navigation }) => {
    const [id, setId] = React.useState("");
    const [firstname, setFirstname] = React.useState("");
    const [lastname, setLastname] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [gender, setGender] = React.useState("male");
    const [address, setAddress] = React.useState("");
    const [mobileNumber, setMobileNumber] = React.useState("");
    const [date, setDate] = React.useState(new Date());
    const [datePickerShow, setDatePickerShow] = React.useState(false);
    const [currentWeight, setCurrentWeight] = React.useState(0);
    const [levelOfFitness, setLevelOfFitness] = React.useState("beginner");
    const [gymAttendance, setGymAttendance] = React.useState("casual");
    const [franchise, setFranchise] = React.useState([]);
    const [image, setImage] = React.useState(null);
    const contextData = React.useContext(AppContext);

    const [visibleAnimation, setVisibleAnimation] = React.useState(false);
    const [visibleAnimatedLoader, setVisibleAnimatedLoader] = React.useState(false);

    const [errorFirstname, setErrorFirstname] = React.useState("");
    const [errorLastname, setErrorLastname] = React.useState("");
    const [errorEmail, setErrorEmail] = React.useState("");
    const [errorAddress, setErrorAddress] = React.useState("");
    const [errorMobileNumber, setErrorMobileNumber] = React.useState("");
    const [errorDate, setErrorDate] = React.useState("");
    const [errorCurrentWeight, setErrorCurrentWeight] = React.useState("");

    useFocusEffect(
        React.useCallback(() => {
            userAsyncDataFetch();
        }, [])
    );

    let userAsyncDataFetch = async () => {
        let userParsedData = await AsyncStorage.getItem("user");
        let user_data = JSON.parse(userParsedData);

        setId(user_data.id);
        setFirstname(user_data.first_name);
        setLastname(user_data.last_name);
        setEmail(user_data.email);
        setImage(user_data.image);
        setGender(user_data.gender);
        setAddress(user_data.address);
        setMobileNumber(user_data.mobile_number);
        setDate(new Date(user_data.birthdate));
        setCurrentWeight(user_data.weight.weight);
        setLevelOfFitness(user_data.athlete_profile.level_of_fitness);
        setGymAttendance(user_data.athlete_profile.gym_attendance);
        setFranchise((user_data.franchise) ? user_data.franchise : []);
    };

    const userData = {
        first_name: firstname,
        last_name: lastname,
        email: email,
        image: image,
        gender: gender,
        address: address,
        mobile_number: mobileNumber,
        birthdate: moment(date).format("LL"),
        current_weight: currentWeight,
        level_of_fitness: levelOfFitness,
        gym_attendance: gymAttendance
    };

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            base64: true,
            aspect: [1, 1],
            quality: 1,
        });

        if (!result.canceled) {
            setImage("data:image/jpeg;base64,"+result.assets[0].base64);
        }
    };

    const validateData = () => {
        (firstname.length < 2) ? setErrorFirstname("First name must be two or more characters.") : setErrorFirstname("");
        
        (lastname.length < 2) ? setErrorLastname("Last name must be two or more characters.") : setErrorLastname("");

        let emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
        (!emailRegex.test(String(email).toLowerCase())) ? setErrorEmail("Please use a valid email.") : setErrorEmail("");

        (address.length < 2) ? setErrorAddress("Address must be two or more characters.") : setErrorAddress("");
        (mobileNumber.length < 2) ? setErrorMobileNumber("Mobile number must be two or more characters.") : setErrorMobileNumber("");
        (currentWeight <= 0) ? setErrorCurrentWeight("Weight must be set.") : setErrorCurrentWeight("");

        moment(date).isAfter(new Date()) ? setErrorDate("Date must not exceed the current date.") : (moment(date).format("LL") === moment(new Date()).format("LL")) ? setErrorDate("Date must not be equal to current date.") : setErrorDate("");

        (firstname.length >= 2 && 
        lastname.length >= 2 && 
        emailRegex.test(String(email).toLowerCase()) && 
        address.length >= 2 && 
        mobileNumber.length >= 2 && 
        currentWeight > 0  && 
        moment(date).isBefore(new Date()) && 
        moment(date).format("LL") !== moment(new Date()).format("LL")) ? updateUser() : '';
    };

    let updateUser = async () => {
        setVisibleAnimatedLoader(!visibleAnimatedLoader);
        setTimeout(() => {
            setVisibleAnimation(!visibleAnimation);
        }, 100);
        setTimeout(() => {
            setVisibleAnimatedLoader(!visibleAnimatedLoader);
        }, 2200);

        await axios(contextData.userAccessTokenContext)
            .put(`/user/update`, userData)
            .then(async (res) => {
                await AsyncStorage.removeItem("user");
                const jsonValue = JSON.stringify(res.data);
                contextData.setUserDataContext(res.data);
                await AsyncStorage.setItem("user", jsonValue);
                userProfileScreen();
            })
            .catch((err) => { 
                console.log('Something went wrong please try again.');
            });
    };

    const userProfileScreen = () => {
        navigation.replace("UserProfileScreen");
    };

    const gymSelectionScreen = () => {
        navigation.navigate("GymSelectionScreen", userData);
    };

    if(visibleAnimation) {
        return (
            <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={{ flex: 1, alignContent: "center", justifyContent: "center" }}>
                <Text style={styles.headerMsg}>All Set!</Text>
                <View style={{ width: 400, height: 400, marginTop: 10 }}>
                    <AnimatedLoader
                        visible={visibleAnimatedLoader}
                        overlayColor="none"
                        animationStyle={{ width: 400, height: 400, marginTop: 10 }}
                        source={require("../../../assets/approved.json")}
                        speed={0.5}
                        loop={false}
                    ></AnimatedLoader>
                </View>
            </LinearGradient>
        );
    }

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
            <SafeAreaView>
                <ScrollView>
                    <KeyboardAvoidingView style={styles.mainContainer} behavior={Platform.OS === "ios" ? "padding" : undefined} keyboardVerticalOffset={55} enabled>
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <>
                                <TopNavigator refresh={true} subpage="true" />
                                <View style={styles.card}>
                                    <Text style={styles.cardHeadline}>Personal Information</Text>
                                    <View style={[styles.flxDrtnColumn, styles.zIndex2]}>
                                        <View style={[styles.alignContentCenter, styles.justifyContentCenter, styles.marginBottom5]}>
                                            <TouchableOpacity style={styles.imagePickerContainer} onPress={pickImage}>
                                                <Image source={(image) ? { uri: image } : require("../../../assets/black-pic.png")} style={styles.imagePicker} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={[styles.zIndex2]}>
                                            <TextInput
                                                style={styles.textInput}
                                                placeholder="First name"
                                                underlineColor="transparent"
                                                selectionColor="black"
                                                activeUnderlineColor="transparent"
                                                value={firstname}
                                                onChangeText={(text) => setFirstname(text)}
                                            />
                                            {errorFirstname != "" && (
                                                <Text style={styles.errorMessage}>{errorFirstname}</Text>
                                            )}
                                            <TextInput
                                                style={styles.textInput}
                                                placeholder="Last name"
                                                underlineColor="transparent"
                                                selectionColor="black"
                                                activeUnderlineColor="transparent"
                                                value={lastname}
                                                onChangeText={(text) => setLastname(text)}
                                            />
                                            {errorLastname != "" && (
                                                <Text style={styles.errorMessage}>{errorLastname}</Text>
                                            )}
                                            <TextInput
                                                style={styles.textInput}
                                                placeholder="Email"
                                                autoCapitalize="none"
                                                underlineColor="transparent"
                                                selectionColor="black"
                                                activeUnderlineColor="transparent"
                                                value={email}
                                                onChangeText={(text) => setEmail(text)}
                                            />
                                            {errorEmail != "" && (
                                                <Text style={styles.errorMessage}>{errorEmail}</Text>
                                            )}
                                        </View>
                                    </View>
                                    <View style={styles.zIndex2}>
                                        <View style={styles.inputContainer}>
                                            <Text style={[styles.width40Percent, styles.marginLeft15, styles.blackText]}>
                                                Date of Birth
                                            </Text>
                                            <TouchableOpacity style={styles.dateButton} onPress={() => { setDatePickerShow(true); }}>
                                                <Text>{moment(date).format("LL")}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        {errorDate != "" && (
                                            <Text style={styles.errorMessage}>{errorDate}</Text>
                                        )}
                                        {datePickerShow && Platform.OS == "ios" && (
                                            <>
                                                <DateTimePicker
                                                    testID="dateTimePicker"
                                                    value={date}
                                                    mode={'date'}
                                                    display="inline"
                                                    onChange={(event, selectedDate) => {
                                                        setDate(selectedDate);
                                                    }}
                                                />
                                                <TouchableOpacity style={[styles.blackLimeOutlineBtn, { width: "auto", marginTop: 0 }]} onPress={() => { setDatePickerShow(false); }}>
                                                    <Text style={styles.limeText}>Confirm Date</Text>
                                                </TouchableOpacity>
                                            </>
                                        )}
                                        <View style={[styles.selectionContainer, styles.selectionContainerCustom]}>
                                            <TouchableOpacity style={[styles.tabSelection, (gender == "male") ? styles.bgBlack : styles.bgTransparent]} onPress={() => { setGender("male") }}>
                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (gender == "male") ? styles.tabTextBold : styles.tabText]}>Male</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (gender == "female") ? styles.bgBlack : styles.bgTransparent]} onPress={() => { setGender("female") }}>
                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (gender == "female") ? styles.tabTextBold : styles.tabText]}>Female</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (gender == "not defined") ? styles.bgBlack : styles.bgTransparent]} onPress={() => { setGender("not defined") }}>
                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (gender == "not defined") ? styles.tabTextBold : styles.tabText]}>Better not say</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.card}>
                                    <Text style={styles.cardHeadline}>Contact Information</Text>
                                    <View style={styles.zIndex2}>
                                        <TextInput
                                            style={styles.textInput}
                                            placeholder="Address (Suburb)"
                                            underlineColor="transparent"
                                            selectionColor="black"
                                            activeUnderlineColor="transparent"
                                            value={address}
                                            onChangeText={(text) => setAddress(text)}
                                        />
                                        {errorAddress != "" && (
                                            <Text style={styles.errorMessage}>{errorAddress}</Text>
                                        )}
                                        <TextInput
                                            style={styles.textInput}
                                            placeholder="Phone/Mobile no."
                                            underlineColor="transparent"
                                            selectionColor="black"
                                            activeUnderlineColor="transparent"
                                            keyboardType="numeric"
                                            value={mobileNumber}
                                            onChangeText={(text) => setMobileNumber(text)}
                                        />
                                        {errorMobileNumber != "" && (
                                            <Text style={styles.errorMessage}>{errorMobileNumber}</Text>
                                        )}
                                    </View>
                                </View>
                                <View style={styles.card}>
                                    <Text style={styles.cardHeadline}>Athlete Profile</Text>
                                    <View style={styles.inputContainer}>
                                        <Text style={[styles.width40Percent, styles.marginLeft15, styles.blackText]}>
                                            Current Weight in kg
                                        </Text>
                                        <TextInput
                                            style={[styles.textInput, styles.textInputCustom]}
                                            placeholder="0"
                                            underlineColor="transparent"
                                            selectionColor="black"
                                            activeUnderlineColor="transparent"
                                            keyboardType="numeric"
                                            value={currentWeight}
                                            onChangeText={(text) => setCurrentWeight(text)}
                                        />
                                    </View>
                                    {errorCurrentWeight != "" && (
                                        <Text style={styles.errorMessage}>{errorCurrentWeight}</Text>
                                    )}
                                    <Text style={styles.cardHeadline}>Level of Fitness</Text>
                                    <View style={[styles.selectionContainer, styles.selectionContainerCustom]}>
                                        <TouchableOpacity style={[styles.tabSelection, (levelOfFitness == "beginner") ? styles.bgBlack : styles.bgTransparent]} onPress={() => setLevelOfFitness("beginner") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (levelOfFitness == "beginner") ? styles.tabTextBold : styles.tabText]}>Beginner</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (levelOfFitness == "intermediate") ? styles.bgBlack : styles.bgTransparent]} onPress={() => setLevelOfFitness("intermediate") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (levelOfFitness == "intermediate") ? styles.tabTextBold : styles.tabText]}>Intermediate</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (levelOfFitness == "advanced") ? styles.bgBlack : styles.bgTransparent]} onPress={() => setLevelOfFitness("advanced") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (levelOfFitness == "advanced") ? styles.tabTextBold : styles.tabText]}>Advanced</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Text style={styles.cardHeadline}>Gym Attendance</Text>
                                    <View style={styles.selectionContainer2}>
                                        <TouchableOpacity style={[styles.tabSelection2, styles.marginBottom5, (gymAttendance == "casual") ? styles.bgBlack : styles.bgLime]} onPress={() => setGymAttendance("casual") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[(gymAttendance == "casual") ? styles.tabSelectionLimeText : styles.tabSelectionBlackText]}>Casual</Text>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingLeft25, (gymAttendance == "casual") ? styles.limeText : styles.blackText]}>1-2 days per week</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.tabSelection2, styles.marginBottom5, (gymAttendance == "regular") ? styles.bgBlack : styles.bgLime]} onPress={() => setGymAttendance("regular") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[(gymAttendance == "regular") ? styles.tabSelectionLimeText : styles.tabSelectionBlackText]}>Regular</Text>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingLeft25, (gymAttendance == "regular") ? styles.limeText : styles.blackText]}>3-4 days per week</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.tabSelection2, (gymAttendance == "enthusiast") ? styles.bgBlack : styles.bgLime]} onPress={() => setGymAttendance("enthusiast") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[(gymAttendance == "enthusiast") ? styles.tabSelectionLimeText : styles.tabSelectionBlackText]}>Enthusiast</Text>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingLeft25, (gymAttendance == "enthusiast") ? styles.limeText : styles.blackText]}>5-7 days per week</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={styles.cardNoBg}>
                                    <Text style={[styles.cardHeadline]}>Franchise</Text>
                                </View>
                                {Object.keys(franchise).length > 0 && (
                                    <View style={[styles.card, styles.marginTop10]}>
                                        <View style={styles.flxDrtnRow}>
                                            <View style={styles.width50Percent}>
                                                <Text style={styles.cardHeadline}>{franchise.name}</Text>
                                                <Text style={styles.grayText}>
                                                    {franchise.address}
                                                </Text>
                                            </View>
                                            <View style={styles.franchiseImageContainer}>
                                                <Image source={franchise.imagePath} style={styles.franchiseImage} />
                                            </View>
                                        </View>
                                    </View>
                                )}
                                <TouchableOpacity style={[styles.transparentBtn, styles.marginBottom0]} onPress={gymSelectionScreen}>
                                    <Text style={styles.whiteText}>SELECT GYM</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.transparentBtn} onPress={validateData}>
                                    <Text style={styles.whiteText}>UPDATE</Text>
                                </TouchableOpacity>
                                {datePickerShow && Platform.OS == "android" && (
                                    <DateTimePicker
                                        testID="dateTimePicker"
                                        value={date}
                                        mode={'date'}
                                        onChange={(event, selectedDate) => {
                                            setDatePickerShow(false);
                                            setDate(selectedDate);
                                        }}
                                    />
                                )}
                            </>
                        </TouchableWithoutFeedback>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default EditProfileScreen;