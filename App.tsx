import React from "react";
import { DefaultTheme, Provider } from "react-native-paper";
import RouteStack from "./routes/RouteStack";

import { AppContext, AppProvider } from "context/AppContext";

const theme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		primary: "#509D96",
		backgroundColor: "white",
	},
};

export default function App() {
	return (
		<AppProvider>
			<Provider theme={theme}>
				<RouteStack />
			</Provider>
		</AppProvider>
	);
}