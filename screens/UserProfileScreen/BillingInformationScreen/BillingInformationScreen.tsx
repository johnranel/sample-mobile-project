import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
    TouchableOpacity,
    Alert,
    ActivityIndicator,
    Modal,
    Switch
} from "react-native";

import { Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { useFocusEffect } from '@react-navigation/native';

import AnimatedLoader from "react-native-animated-loader";
import TopNavigator from "screens/TopNavigator/TopNavigator";

import styles from "./BillingInformationScreenStyles.styles";

import axios from "services/api"
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AppContext } from "../../../context/AppContext";

import { StripeProvider, useStripe } from "@stripe/stripe-react-native";
import moment from 'moment';

const BillingInformationScreen = () => {
    const [listItems, setListItems] = React.useState([]);
    const [fullName, setFullName] = React.useState("");
    const [userId, setUserId] = React.useState("");
    const [userPoints, setUserPoints] = React.useState(0);
    const [userBillingAddress, setUserBillingAddress] = React.useState("");

    const [offsetTransactionData, setOffsetTransactionData] = React.useState(0);
    const [lockVerticalLoading, setLockVerticalLoading] = React.useState(false);
    const [dataLoading, setDataLoading] = React.useState(false);

    const [modalVisibility, setModalVisibility] = React.useState(false);
    const [visibleAnimatedLoader, setVisibleAnimatedLoader] = React.useState(false);
    const [paymentStatus, setPaymentStatus] = React.useState(true);

    const contextData = React.useContext(AppContext);
    const stripe = useStripe();

    const [screenFocused, setScreenFocused] = React.useState(false);

    const [autoBillingEnable, setAutoBillingEnable] = React.useState(false);
    const [autoBillingAmount, setAutoBillingAmount] = React.useState(30);
    const [autoBillingTiming, setAutoBillingTiming] = React.useState("weekly");

    useFocusEffect(
        React.useCallback(() => {
            if(screenFocused === false) {
                userAsyncDataFetch();
            }
            return () => {
                setScreenFocused(true);
            };
        }, [screenFocused])
    );

    let userAsyncDataFetch = async () => {
        let userParsedData = await AsyncStorage.getItem("user");
        let user_points = await AsyncStorage.getItem("user_points");
        let user_data = JSON.parse(userParsedData);
        let user_points_data = JSON.parse(user_points);
        setUserId(user_data.id);
        setFullName(user_data.first_name + " " + user_data.last_name);
        setUserBillingAddress(user_data.address);
        setUserPoints(user_points_data);

        if(user_data.auto_billing) {
            setAutoBillingEnable(user_data.auto_billing.enable_billing);
            setAutoBillingAmount(parseInt(user_data.auto_billing.amount));
            setAutoBillingTiming(user_data.auto_billing.timing);
        }

        await topUpDataFetch(user_data.id);
    };

    let topUpDataFetch = async (user_id) => {
        setDataLoading(true);
        setLockVerticalLoading(true);
        await axios(contextData.userAccessTokenContext)
            .get(`/user/transaction/get/limit/${offsetTransactionData}/type/top-up`)
            .then(async (res) => {
                setDataLoading(false);
                setLockVerticalLoading(false);
                (listItems.length != 0) ? setListItems(listItems.concat(res.data.transaction_data)) : setListItems(res.data.transaction_data);
                setOffsetTransactionData(offsetTransactionData+10);
                if (res.data.transaction_data.length !== 10) {
                    setLockVerticalLoading(true);
                }
            });
    };

    let topUpViaStripe = async (stripe_amount, amount_aud) => {
        setModalVisibility(true);

        let response = await axios(contextData.userAccessTokenContext)
                .post("/pay", { amount: stripe_amount })
                .then(async (res) => {
                    return res.data;
                })
                .catch((err) => {
                    return animationPaymentStatus(false);
                });
        
        
        const clientSecret = response.client_secret;
        const customerId = response.customer_id;

        const initSheet = await stripe.initPaymentSheet({ customerId: customerId, paymentIntentClientSecret: clientSecret, merchantDisplayName: 'Supplement Station' });
        if(initSheet.error) {
            return setModalVisibility(false);
        }

        const presenSheet = await stripe.presentPaymentSheet();
        if(presenSheet.error) {
            return setModalVisibility(false);
        }

        await axios(contextData.userAccessTokenContext)
            .post("/save/transaction", { type: 'top-up', amount: amount_aud, description: fullName + ' has topped up ' + amount_aud + ' AUD.', stripe_client_secret: clientSecret })
            .then(async (res) => {
                setUserPoints(res.data.points_data);
                let userPointsDataString = JSON.stringify(res.data.points_data);
                await AsyncStorage.setItem("user_points", userPointsDataString);
                animationPaymentStatus(true);
            })
            .catch(() => {
                return animationPaymentStatus(false);
            });
    }

    let setUpAutoBilling = async () => {
        setModalVisibility(true);

        if(autoBillingEnable) {
            let response = await axios(contextData.userAccessTokenContext)
                    .post("/link/payment/card")
                    .then(async (res) => {
                        return res.data;
                    })
                    .catch((err) => {
                        return animationPaymentStatus(false);
                    });

            const setupIntentId = response.setup_intent_id;
            const customerId = response.customer_id;

            const initSheet = await stripe.initPaymentSheet({ customerId: customerId, setupIntentClientSecret: setupIntentId, merchantDisplayName: 'Supplement Station' });
            if(initSheet.error) {
                return setModalVisibility(false);
            }

            const presenSheet = await stripe.presentPaymentSheet();
            if(presenSheet.error) {
                return setModalVisibility(false);
            }
        }

        await axios(contextData.userAccessTokenContext)
            .post("/auto-billing", { amount: autoBillingAmount, timing: autoBillingTiming, enable_billing: autoBillingEnable })
            .then(async (res) => {
                let userDataString = JSON.stringify(res.data.user);
                await AsyncStorage.setItem("user", userDataString);
                animationPaymentStatus(true);
            })
            .catch(() => {
                return animationPaymentStatus(false);
            });
    }

    let animationPaymentStatus = async (status) => {
        setVisibleAnimatedLoader(true);
        setPaymentStatus(status);
        setTimeout(() => {
            setModalVisibility(false);
            setVisibleAnimatedLoader(false);
        }, 2000);
    };

    let isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        if(!lockVerticalLoading) {
            let paddingToBottom = 10;
            return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
        } else {
            return false;
        }
    };

    return (
        <>
            <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
                <SafeAreaView style={styles.mainContainer}>
                    <ScrollView style={styles.mainContainer}
                        onScroll={({nativeEvent}) => {
                            if (isCloseToBottom(nativeEvent)) {
                                topUpDataFetch(userId);
                            }
                        }}>
                        <View style={styles.alignItemsCenter}>
                            <TopNavigator refresh={true} subpage="true" />
                            <View style={styles.cardNoBg}>
                                <Text style={[styles.cardHeadline, styles.marginBottom0]}>Billing Information</Text>
                            </View>
                            <View style={styles.flxDrtnRow}>
                                <View style={styles.balanceHeadlineContainer}>
                                    <Text style={styles.cardHeadline}>Account Balance</Text>
                                </View>
                                <View style={styles.balanceContainer}>
                                    <View style={styles.accountBalance}>
                                        <Text style={[styles.limeBoldText, styles.fontSize20]}>Points</Text>
                                        <Text style={[styles.limeBoldText, styles.fontSize40]}>{(userPoints != null) ? userPoints.points : 0}</Text>
                                        <Text style={[styles.limeBoldText, styles.fontSize20]}>{(userPoints != null) ? userPoints.amount : 0} AUD</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.cardNoBg}>
                                <Text style={styles.cardHeadline}>Top-ups</Text>
                            </View>
                            <View style={styles.flxDrtnRow}>
                                <TouchableOpacity style={[styles.boxCard, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={() => topUpViaStripe(30*100, 30)}>
                                    <Text style={[styles.limeBoldText, styles.fontSize20]}>30</Text>
                                    <Text style={styles.limeBoldText}>AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.boxCard, styles.alignItemsCenter, styles.justifyContentCenter, styles.marginLeft12]} onPress={() => topUpViaStripe(50*100, 50)}>
                                    <Text style={[styles.limeBoldText, styles.fontSize20]}>50</Text>
                                    <Text style={styles.limeBoldText}>AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.boxCard, styles.alignItemsCenter, styles.justifyContentCenter, styles.marginLeft12]} onPress={() => topUpViaStripe(100*100, 100)}>
                                    <Text style={[styles.limeBoldText, styles.fontSize20]}>100</Text>
                                    <Text style={styles.limeBoldText}>AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.boxCard, styles.alignItemsCenter, styles.justifyContentCenter, styles.marginLeft12]} onPress={() => topUpViaStripe(150*100, 150)}>
                                    <Text style={[styles.limeBoldText, styles.fontSize20]}>150</Text>
                                    <Text style={styles.limeBoldText}>AUD</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.cardNoBg, styles.flxDrtnRow, styles.justifyContentSpaceBetween, styles.alignItemsCenter]}>
                                <Text style={styles.cardHeadline}>Auto Top-ups</Text>
                                <View style={[styles.flxDrtnRow, styles.alignItemsCenter]}>
                                    <Switch
                                        trackColor={{ false: "#FFFFFF", true: "#C9EA3B" }}
                                        thumbColor={autoBillingEnable ? "#C9EA3B" : "#666666"}
                                        ios_backgroundColor="#3e3e3e"
                                        onValueChange={() => setAutoBillingEnable(!autoBillingEnable)}
                                        value={autoBillingEnable}
                                    />
                                    {autoBillingEnable == true && (
                                        <Text style={styles.marginLeft5}>On</Text>
                                    )}
                                    {autoBillingEnable == false && (
                                        <Text style={styles.marginLeft5}>Off</Text>
                                    )}
                                </View>
                            </View>
                            <View style={[styles.selectionContainer, styles.selectionContainerCustom]}>
                                <TouchableOpacity style={[styles.tabSelection, (autoBillingAmount == 30) ? styles.bgBlack : styles.bgTransparent]} onPress={() => setAutoBillingAmount(30) }>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (autoBillingAmount == 30) ? styles.tabTextBold : styles.tabText]}>30 AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (autoBillingAmount == 50) ? styles.bgBlack : styles.bgTransparent]} onPress={() => setAutoBillingAmount(50) }>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (autoBillingAmount == 50) ? styles.tabTextBold : styles.tabText]}>50 AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (autoBillingAmount == 100) ? styles.bgBlack : styles.bgTransparent]} onPress={() => setAutoBillingAmount(100) }>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (autoBillingAmount == 100) ? styles.tabTextBold : styles.tabText]}>100 AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (autoBillingAmount == 150) ? styles.bgBlack : styles.bgTransparent]} onPress={() => setAutoBillingAmount(150) }>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (autoBillingAmount == 150) ? styles.tabTextBold : styles.tabText]}>150 AUD</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.selectionContainer, styles.selectionContainerCustom]}>
                                <TouchableOpacity style={[styles.tabSelection, styles.width48Percent, (autoBillingTiming == "weekly") ? styles.bgBlack : styles.bgTransparent]} onPress={() => setAutoBillingTiming("weekly") }>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (autoBillingTiming == "weekly") ? styles.tabTextBold : styles.tabText]}>Weekly</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.tabSelection, styles.width48Percent, styles.marginLeft15, (autoBillingTiming == "monthly") ? styles.bgBlack : styles.bgTransparent]} onPress={() => setAutoBillingTiming("monthly") }>
                                    <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (autoBillingTiming == "monthly") ? styles.tabTextBold : styles.tabText]}>Monthly</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.selectionContainer}>
                                <TouchableOpacity style={styles.transparentBtn} onPress={() => setUpAutoBilling() }>
                                    <Text style={styles.whiteText}>Subscribe</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.cardNoBg}>
                                <Text style={styles.cardHeadline}>Top-up History</Text>
                                <View style={[styles.card, styles.marginTop0]}>
                                    <View style={[styles.flxDrtnRow, styles.padding5]}>
                                        <View style={styles.width20Percent}>
                                            <Text style={[styles.blackBoldText, styles.textAlignCenter]}>Date</Text>
                                        </View>
                                        <View style={styles.width40Percent}>
                                            <Text style={[styles.blackBoldText, styles.textAlignCenter]}>Description</Text>
                                        </View>
                                        <View style={styles.width20Percent}>
                                            <Text style={[styles.blackBoldText, styles.textAlignCenter]}>Points</Text>
                                        </View>
                                        <View style={styles.width20Percent}>
                                            <Text style={[styles.blackBoldText, styles.textAlignCenter]}>Amount (AUD)</Text>
                                        </View>
                                    </View>
                                    <View style={styles.lineDivider}></View>
                                    {listItems.length > 0 && (
                                        <>
                                            {
                                                listItems.map((item, index) => {
                                                    return (
                                                        <View key={"top"+item.id}>
                                                            <View style={[styles.flxDrtnRow, styles.padding5]}>
                                                                <View style={styles.width20Percent}>
                                                                    <Text style={[styles.grayText, styles.textAlignCenter]}>{moment(item.created_at).format("MMM DD")}</Text>
                                                                </View>
                                                                <View style={styles.width40Percent}>
                                                                    <Text style={styles.textAlignCenter}>Topped-up via the app.</Text>
                                                                </View>
                                                                <View style={styles.width20Percent}>
                                                                    <Text style={styles.textAlignCenter}>{item.points}</Text>
                                                                </View>
                                                                <View style={styles.width20Percent}>
                                                                    <Text style={styles.textAlignCenter}>{item.amount}</Text>
                                                                </View>
                                                            </View>
                                                            <View style={{ borderBottomWidth: 1, borderColor: "#ddd" }}></View>
                                                        </View>
                                                    );
                                                })
                                            }
                                        </>
                                    )}
                                    {dataLoading && (
                                        <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                                    )}
                                    {listItems.length <= 0 && dataLoading == false && (
                                        <Text style={styles.alignSelfCenter}>No available top-ups.</Text>
                                    )}
                                </View>
                            </View>
                            <View style={[styles.cardNoBg, {marginBottom: 20}]}>
                                <Text style={[styles.cardHeadline]}>Billing Address</Text>
                                <Text style={styles.cardDescription}>{userBillingAddress}</Text>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
                <StripeProvider publishableKey="pk_test_UE5jihFopoDx5Ka14j3VrVyW"></StripeProvider>
            </LinearGradient>
            <Modal animationType="slide"
                transparent={true}
                visible={modalVisibility}
                >
                    <View style={{ display: "flex", flex: 1, backgroundColor: "black", opacity: 0.6, alignItems: "center", alignContent: "center", justifyContent: "center" }}>
                        {!visibleAnimatedLoader && (
                            <>
                                <ActivityIndicator size="large" color="#C9EA3B" />
                                <Text style={{ color: "#C9EA3B" }}>Loading...</Text>
                            </>
                        )}
                        {visibleAnimatedLoader && (
                            <AnimatedLoader
                                visible={true}
                                overlayColor="none"
                                animationStyle={{ width: 400, height: 400, marginTop: 10 }}
                                source={(paymentStatus) ? require("../../../assets/payment-successful.json") : require("../../../assets/error-failure.json")}
                                speed={0.5}
                                loop={false}
                            ></AnimatedLoader>
                        )}
                    </View>
            </Modal>
        </>
    );
};

export default BillingInformationScreen;