import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { StatusBar } from "react-native";
import * as Linking from 'expo-linking';

import LoginScreen from "../screens/LoginScreen/LoginScreen";
import RegistrationScreen from "../screens/RegistrationScreen/RegistrationScreen";
import GymSelectionScreen from "../screens/RegistrationScreen/GymSelectionScreen/GymSelectionScreen";
import ForgotPasswordScreen from "screens/ForgotPasswordScreen/ForgotPasswordScreen";
import ResetPasswordScreen from "screens/ResetPasswordScreen/ResetPasswordScreen";

import BottomNavigator from "../screens/BottomNavigator/BottomNavigator";

import MenuScreen from "../screens/MenuScreen/MenuScreen";
import ContactUsScreen from "../screens/MenuScreen/ContactUsScreen/ContactUsScreen";
import BecomeAPartnerScreen from "../screens/MenuScreen/BecomeAPartnerScreen/BecomeAPartnerScreen";
import FAQScreen from "../screens/MenuScreen/FAQScreen/FAQScreen";
import TermsAndConditionScreen from "../screens/MenuScreen/TermsAndConditionScreen/TermsAndConditionScreen";
import PrivacyPolicyScreen from "../screens/MenuScreen/PrivacyPolicyScreen/PrivacyPolicyScreen";

import { AppContext } from "../context/AppContext";
const Stack = createStackNavigator();
const prefixes = Linking.makeUrl("/");

export default function RouteStack() {
    const contextData = React.useContext(AppContext);
    const [deepLink, setDataDeepLink] = React.useState();
    
    const linking = {
        prefixes: [prefixes],
        config: {
            screens: {
                ForgotPasswordScreen: 'forgot-password',
                ResetPasswordScreen: 'reset-password',
            }
        },
    }

    React.useEffect(() => {
        async function getInitialUrl() {
            const initialUrl = await Linking.getInitialURL();
            if(initialUrl) setDataDeepLink(initialUrl);
        }

        Linking.addEventListener("url", handleDeepLink);
        if(!deepLink) {
            getInitialUrl();
        }
    });

    let handleDeepLink = async (event) => {
        let deepLinkParsed = Linking.parse(event.url);
        setDataDeepLink(deepLinkParsed);
    }

    return (
        <NavigationContainer linking={linking}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <Stack.Navigator
                screenOptions={{
                    headerStyle: { elevation: 0 },
                    cardStyle: { backgroundColor: "#fff" },
                }}
            >
                {!contextData.userAccessTokenContext ? (
                    <>
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="LoginScreen"
                            component={LoginScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="RegistrationScreen"
                            component={RegistrationScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="GymSelectionScreen"
                            component={GymSelectionScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="ForgotPasswordScreen"
                            component={ForgotPasswordScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="ResetPasswordScreen"
                            component={ResetPasswordScreen}
                        />
                    </>
                ) : (
                    <>
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="BottomNavigator"
                            component={BottomNavigator}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="MenuScreen"
                            component={MenuScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="ContactUsScreen"
                            component={ContactUsScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="BecomeAPartnerScreen"
                            component={BecomeAPartnerScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="FAQScreen"
                            component={FAQScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="TermsAndConditionScreen"
                            component={TermsAndConditionScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="PrivacyPolicyScreen"
                            component={PrivacyPolicyScreen}
                        />
                    </>
                )}
            </Stack.Navigator>
        </NavigationContainer>
    );
}