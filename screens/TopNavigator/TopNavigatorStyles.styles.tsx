import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
    topBarContainer: {
        flexDirection: "row",
        alignContent: "center",
        justifyContent: "center",
        paddingTop: 5
    },

    topBarItems: {
        width: (Dimensions.get('window').width - 25) / 3,
    }
});