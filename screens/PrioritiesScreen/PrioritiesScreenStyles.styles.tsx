import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    gradientContainer: {
        flex: 1
    },

    mainContainer: {
        flex: 1, 
        alignItems: "center"
    },

    weightBtn: {
        height: 40, 
        width: 40, 
        marginLeft: 15, 
        backgroundColor: "white", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#36b9cc"
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 25,
    },

    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    cardSubHeadline: {
        fontSize: 14,
        fontWeight: "bold",
        color: "#ccc",
        marginBottom: 10,
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    textInput: {
        backgroundColor: "transparent",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
        color: "black",
        borderColor: "#36b9cc",
        borderWidth: 1.5,
        marginBottom: 5,
        height: 48,
    },

    categoryTabs: {
        flexDirection: "row", 
        height: 40, 
        width: "32.5%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25, 
    },

    borderGrayCustom: {
        borderWidth: 1.5, 
        borderColor: "#ccc"
    },

    bdrBgLimeCustom: {
        borderWidth: 1.5, 
        borderColor: "#36b9cc",
        backgroundColor: "black"
    },

    limeBoldCenterText: {
        color: "#36b9cc",
        fontSize: 14,
        fontWeight: "bold",
        textAlign: "center"
    },

    limeBoldText: {
        color: "#36b9cc",
        fontSize: 14,
        fontWeight: "bold",
    },

    blackCenterText: {
        color: "black",
        fontSize: 14,
        textAlign: "center"
    },

    blackText: {
        color: "black",
        fontSize: 14,
    },

    switchContainer: {
        flexDirection: "row", 
        marginBottom: 5, 
        height: 40, 
        alignItems: "center", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#ccc"
    },

    switchSelection: {
        height: 40, 
        width: "48.2%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    favoritesBtn: {
        flexDirection: "row", 
        paddingVertical: 2.5, 
        paddingHorizontal: 10, 
        borderWidth: 1.5, 
        borderColor: "#36b9cc", 
        borderRadius: 15, 
        alignContent: "center", 
        justifyContent: "center",
        width: 100
    },

    productImage: {
        height: 100, 
        width: "100%", 
        // alignSelf: "center", 
        // borderRadius: 20,
        resizeMode: "contain"
    },

    productName: {
        // width: "55%", 
        // alignItems: "center", 
        // justifyContent: "center"
        textAlign: "center", 
        fontWeight: "bold", 
        color: "#676767", 
        marginTop: 5
    },

    productDescription: {
        width: "15%", 
        alignItems: "center", 
        justifyContent: "center"
    },

    submitBtn: {
        height: 40, 
        width: "49%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25, 
        backgroundColor: "black", 
        borderWidth: 1.5, 
        borderColor: "#36b9cc"
    },

    closeBtn: {
        height: 40, 
        width: "49%", 
        marginLeft: 5, 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#ccc"
    },

    productContainer: {
        width: (Dimensions.get('window').width / 3) - 22, 
        marginBottom: 10, 
        padding: 5 
    },

    productInfo: {
        position: "absolute", 
        right: 15, 
        zIndex: 2, 
        paddingVertical: 5, 
        paddingHorizontal: 10, 
        backgroundColor: "#36b9cc", 
        borderRadius: 20, 
        alignItems: "center", 
        flexDirection: "row"
    },

    marginVertical0: {
        marginVertical: 0
    },

    marginTop10: {
        marginTop: 10
    },

    marginRight5: {
        marginRight: 5
    },

    marginLeft5 : {
        marginLeft: 5
    },

    marginLeft15 : {
        marginLeft: 15
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    flxWrap: {
        flexWrap: "wrap"
    },

    flex1: {
        flex: 1
    },

    marginBottom5: {
        marginBottom: 5
    },

    marginBottom10: {
        marginBottom: 10
    },

    marginBottom20: {
        marginBottom: 20
    },

    padding5: {
        padding: 5
    },

    alignItemsCenter: {
        alignItems: "center"
    },

    width50Percent: {
        width: "50%",
    },

    width80Percent: {
        width: "80%",
    },

    bgBlack: {
        backgroundColor: "black"
    },

    bgTransparent: {
        backgroundColor: "transparent"
    },

    textAlignRight: {
        textAlign: "right"
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    justifyContentCenter: {
        justifyContent: "center"
    },

    textBold: {
        fontWeight: "bold"
    },

    paddingHorizontal10: {
        paddingHorizontal: 10
    },

    fontSize11: {
        fontSize: 11
    },

    justifyContentSpaceBetween: {
        justifyContent: "space-between"
    },

    paddingX5: {
        paddingLeft: 5,
        paddingRight: 5
    }
    
});