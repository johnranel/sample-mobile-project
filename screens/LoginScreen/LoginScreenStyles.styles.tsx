import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    mainContainer: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },

    card: {
        backgroundColor: "#ffffff", 
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    container: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 2,
    },

    imgStyle: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 30,
        height: 120,
        resizeMode: "contain"
    },

    passwordInputContainer: {
        backgroundColor: "#CFFF00",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
    },

    textInput: {
        backgroundColor: "#CFFF00",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
        paddingLeft: 25,
        paddingRight: 25,
        color: "black",
        borderColor: "white",
        marginBottom: 5,
        height: 48,
    },
    
    rememberForgotContainer: {
        flexDirection: "row", 
        marginHorizontal: 40, 
        alignItems: "center", 
        marginBottom: 15
    },

    checkboxContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },

    checkbox: {
        margin: 8,
    },

    checkboxText: {
        color: "#676767",
        fontSize: 14,
        marginTop: 10,
        marginBottom: 10,
    },

    forgotPassword: {
        color: "#676767",
        fontSize: 14,
        textAlign: "right",
        margin: 8
    },

    blackLimeOutlineBtn: {
        height: 40,
        width: 149,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#13181b",
        borderRadius: 25,
        borderColor: "#36b9cc",
        borderWidth: 1.5,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    limeText: {
        color: "#36b9cc",
        fontSize: 14,
    },

    signUpText: {
        color: "#676767",
        fontSize: 14,
        marginTop: 25,
        marginBottom: 15,
    },

    signUpSubText: {
        color: "black",
        textDecorationLine: "underline",
        fontSize: 14,
        marginTop: 25,
        marginBottom: 15,
    },

    loadingContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },

    loadingText: {
        textAlign: "center",
    },

    justifyContainer: {
        flex: 1, 
        justifyContent: "flex-start",
    },

    errorContainer: {
        margin: 5, 
        alignSelf: "center"
    },

    errorMessage: {
        color: "red",
        fontSize: 14,
    },

    fieldsContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },

    passwordViewContainer: {
        width: "10%"
    },

    width90percent: {
        width: "90%"
    },

    marginBottom0: {
        marginBottom: 0
    },
});