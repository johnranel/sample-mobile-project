import React from "react";

import {
    View,
    TouchableOpacity,
    Image
} from "react-native";

import { Text } from "react-native-paper";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useFocusEffect } from '@react-navigation/native';

import styles from "./TopNavigatorStyles.styles";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { useNavigation } from '@react-navigation/native';

const TopNavigator = ({ refresh, subpage }) => {
    const [profilePicture, setProfilePicture] = React.useState("");
    const [userPoints, setUserPoints] = React.useState(0);
    const [topNavigatorLoaded, setTopNavigatorLoaded] = React.useState(false);
    const navigation = useNavigation();

    let menuScreen = () => {
        navigation.navigate("MenuScreen");
    };

    let editProfileScreen = () => {
        navigation.navigate("EditProfileScreen");
    };

    let goBack = () => {
        navigation.goBack();
    };

    useFocusEffect(
        React.useCallback(() => {
            userAsyncDataFetch();
        }, [refresh])
    );

    let userAsyncDataFetch = async () => {
        let userParsedData = await AsyncStorage.getItem("user");
        let user_data = JSON.parse(userParsedData);
        setProfilePicture(user_data.image);
    
        let userPointsData = await AsyncStorage.getItem("user_points");
        let userPointsParsed = JSON.parse(userPointsData);
        setUserPoints(userPointsParsed.points);
    };

    return (
        <View style={styles.topBarContainer}>
            {subpage == 'false' && (
                <View style={styles.topBarItems}>
                    <TouchableOpacity style={{ padding: 5, backgroundColor: "black", width: 36, borderRadius: 10 }} onPress={menuScreen}>
                        <MaterialCommunityIcons
                            style={{ marginRight: 2 }}
                            name={"menu"}
                            color={"#36b9cc"}
                            size={25}
                        />
                    </TouchableOpacity>
                </View>
            )}
            {subpage == 'true' && (
                <View style={styles.topBarItems}>
                    <TouchableOpacity style={{ padding: 5, backgroundColor: "black", width: 36, borderRadius: 10 }} onPress={goBack}>
                        <MaterialCommunityIcons
                            style={{ marginRight: 2 }}
                            name={"arrow-left"}
                            color={"#36b9cc"}
                            size={25}
                        />
                    </TouchableOpacity>
                </View>
            )}
            <View style={[styles.topBarItems, { flexDirection: "row", alignSelf: "center", justifyContent: "center" }]}>
                <Text style={{ fontWeight: "bold", marginRight: 5 }}>Balance:</Text>
                <Text>{userPoints} Points</Text>
            </View>
            <View style={[styles.topBarItems, { flexDirection: "row", justifyContent: "flex-end" }]}>
                <TouchableOpacity style={[{ height: 38, width: 38, borderRadius: 38 }, (profilePicture == "") ? { borderColor: "#ccc", borderWidth: 1.5 } : {}]} onPress={editProfileScreen}>
                    {profilePicture == "" && (
                        <MaterialCommunityIcons
                            style={{ marginRight: 2 }}
                            name={"account"}
                            color={"black"}
                            size={25}
                        />
                    )}
                    <Image source={(profilePicture) ? { uri: profilePicture } : require("../../assets/black-pic.png")} style={{ width: 38, height: 38, borderRadius: 38, borderColor: "#ccc", borderWidth: 1.5 }} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default TopNavigator;