import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    mainContainer: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    personalInfoContainer: {
        zIndex: 2, 
        flexDirection: "column"
    },

    personalInfoFieldsContainer: {
        zIndex: 2, 
    },

    passwordInputContainer: {
        backgroundColor: "transparent",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
        borderColor: "#C9EA3B",
        borderWidth: 1.5,
        marginBottom: 5,
    },

    textInput: {
        backgroundColor: "transparent",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
        color: "black",
        borderColor: "#C9EA3B",
        borderWidth: 1.5,
        marginBottom: 5,
        height: 48
    },

    countryCode: {
        backgroundColor: "transparent",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
        color: "black",
        borderColor: "#C9EA3B",
        borderWidth: 1.5,
        marginBottom: 5,
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        width: 50,
    },

    passwordViewContainer: {
        width: "10%"
    },

    grayImageContainer: {
        alignContent: "center", 
        justifyContent: "center"
    },

    grayImagePickerBtn: {
        height: 120, 
        width: 120, 
        backgroundColor: "#ebebeb", 
        borderRadius: 120,
        alignSelf: "center"
    },

    grayImagePickerImg: {
        height: 120, 
        width: 120, 
        borderRadius: 120
    },

    selectionContainer: {
        flexDirection: "row", 
        marginBottom: 5, 
        height: 40, 
        alignItems: "center"
    },

    selectionContainerCustom: {
        backgroundColor: "#FBFBFB", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#C9EA3B"
    },

    fieldLabel: {
        marginLeft: 15, 
        fontSize: 14, 
        width: "40%"
    },

    dateCustomBtn: {
        height: 40, 
        marginLeft: 15, 
        width: "50%", 
        backgroundColor: "#CFFF00", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    currentWeightCustomFld: {
        marginLeft: 15, 
        height: 40, 
        width: "50%", 
        backgroundColor: "#CFFF00", 
        borderColor: "#CFFF00",
        textAlign: "center"
    },

    blackLimeOutlineBtn: {
        marginTop: 20,
        marginBottom: 20,
        height: 40,
        width: "90%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#13181b",
        borderRadius: 25,
        borderColor: "#C9EA3B",
        borderWidth: 1.5,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    fieldsContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },

    limeText: {
        color: "#C9EA3B",
        fontSize: 14,
    },

    errorMessage: {
        color: "red",
        fontSize: 14,
        marginBottom: 10
    },

    marginLeft10 : {
        marginLeft: 10,
    },

    alignSelfCenter: {
        alignSelf: "center",
    },

    tabSelection: {
        height: 40, 
        width: "30%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25,
    },

    tabTextBold: {
        color: "#C9EA3B", 
        fontWeight: "bold"
    },

    tabText: {
        color: "black", 
        fontSize: 14,
    },

    tabSelectionFullWidth: {
        flexDirection: "row", 
        height: 40, 
        width: "100%", 
        alignItems: "center", 
        borderRadius: 25,
    },

    tabTextFullWidth: {
        width: "50%", 
        textAlign: "left", 
        paddingLeft: 25,
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    justifyContentSpaceBetween: {
        justifyContent: "space-between"
    },

    marginLeft15: {
        marginLeft: 15,
    },

    marginBottom5: {
        marginBottom: 5,
    },

    marginBottom10: {
        marginBottom: 10,
    },

    bgBlack: {
        backgroundColor: "black",
    },

    bgTransparent: {
        backgroundColor: "transparent",
    },

    bgLime: {
        backgroundColor: "#CFFF00",
    },

    limeTextPad: {
        color: "#C9EA3B",
        fontSize: 14,
        paddingLeft: 25,
    },

    tabTextPad: {
        color: "black",
        fontSize: 14,
        paddingLeft: 25,
    },

    zIndex2: {
        zIndex: 2,
    },

    width90percent: {
        width: "90%"
    },

    width85percent: {
        width: "85%"
    },

    marginBottom0: {
        marginBottom: 0
    },

    borderColorTransparent: {
        borderColor: "transparent",
    },

    paddingX5: {
        paddingLeft: 5,
        paddingRight: 5
    },
});