import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
    TouchableOpacity
} from "react-native";

import { Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";

import styles from "./MenuScreenStyles.styles";

const MenuScreen = ({ navigation }) => {
    const goBack = () => {
        navigation.goBack();
    };

    const contactUsScreen = () => {
        navigation.navigate("ContactUsScreen");
    };

    const becomeAPartnerScreen = () => {
        navigation.navigate("BecomeAPartnerScreen");
    };

    const faqScreen = () => {
        navigation.navigate("FAQScreen");
    };

    const termsAndConditionScreen = () => {
        navigation.navigate("TermsAndConditionScreen");
    };

    const privacyPolicyScreen = () => {
        navigation.navigate("PrivacyPolicyScreen");
    };

    return (
        <LinearGradient colors={['black', 'black', 'black']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView style={styles.mainContainer}>
                    <View style={styles.alignItemsCenter}>
                        <View style={styles.topBarContainer}>
                            <View style={styles.topBarItems}>
                                <TouchableOpacity style={styles.topBarBtn} onPress={goBack}>
                                    <MaterialCommunityIcons
                                        style={styles.marginRight2}
                                        name={"arrow-left"}
                                        color={"black"}
                                        size={25}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.cardNoBg}>
                            <TouchableOpacity onPress={contactUsScreen}>
                                <Text style={styles.whiteBoldText30}>Contact Us</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cardNoBg}>
                            <TouchableOpacity onPress={becomeAPartnerScreen}>
                                <Text style={styles.whiteBoldText30}>Become a Partner</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cardNoBg}>
                            <TouchableOpacity onPress={faqScreen}>
                                <Text style={styles.whiteBoldText30}>FAQs</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cardNoBg}>
                            <TouchableOpacity onPress={termsAndConditionScreen}>
                                <Text style={styles.whiteBoldText30}>Terms & Conditions</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.cardNoBg}>
                            <TouchableOpacity onPress={privacyPolicyScreen}>
                                <Text style={styles.whiteBoldText30}>Privacy Policy</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default MenuScreen;
