import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";

import { Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useFocusEffect } from '@react-navigation/native';
import TopNavigator from "screens/TopNavigator/TopNavigator";

import styles from "./ProductScreenStyles.styles";

import axios from "services/api";
import { AppContext } from "../../context/AppContext";

const ProductScreen = ({ navigation }) => {
    const [listItems, setListItems] = React.useState([]);
    const [flavoursTab, setFlavoursTab] = React.useState(false);
    const [brandsTab, setBrandsTab] = React.useState(true);

    const [offsetProductsData, setOffsetProductsData] = React.useState(0);

    const [lockVerticalLoading, setLockVerticalLoading] = React.useState(false);
    const [noMoreVerticalData, setNoMoreVerticalData] = React.useState(false);

    const [productTypeDefaultArray, setProductTypeDefaultArray] = React.useState([]);
    const [flavoursDefaultArray, setFlavoursDefaultArray] = React.useState([]);
    const [brandsDefaultArray, setBrandsDefaultArray] = React.useState([]);

    const [flavours, setFlavoursData] = React.useState([]);
    const [types, setTypesData] = React.useState([]);
    const [brands, setBrandsData] = React.useState([]);

    const [filteredItems, setFilteredItems] = React.useState([]);
    const [filteredDataLoading, setFilteredDataLoading] = React.useState(false);

    const contextData = React.useContext(AppContext);
    const [screenFocused, setScreenFocused] = React.useState(false);

    const productDetailScreen = (item) => {
        navigation.navigate("ProductDetailScreen", item);
    };

    useFocusEffect(
        React.useCallback(() => {
            if(screenFocused === false) {
                asyncProductsFetch();
                asyncProductTypesAndFlavoursFetch();
            }
            return () => {
                setScreenFocused(true);
            };
        }, [screenFocused])
    );

    let asyncProductsFetch = async () => {
        setLockVerticalLoading(true);
        await axios(contextData.userAccessTokenContext)
            .get(`/products/get/${offsetProductsData}`)
            .then(async(res) => {
                (listItems.length != 0) ? setListItems(listItems.concat(res.data.product_list)) : setListItems(res.data.product_list);
                setOffsetProductsData(offsetProductsData+6);
                setLockVerticalLoading(false);
                if (res.data.product_list.length !== 6) {
                    setNoMoreVerticalData(true);
                }
            }).catch((err) => {
                setLockVerticalLoading(false);
                setNoMoreVerticalData(true);
            });
    };

    let asyncProductTypesAndFlavoursFetch = async () => {
        await axios(contextData.userAccessTokenContext)
            .get(`/product/brand-types-and-flavours/get`)
            .then(async(res) => {
                setProductTypeDefaultArray(res.data.product_types);
                setFlavoursDefaultArray(res.data.product_flavours);
                setBrandsDefaultArray(res.data.product_brands);
            });
    };

    let isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        if(!lockVerticalLoading) {
            let paddingToBottom = 10;
            return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
        } else {
            return false;
        }
    };

    let flavoursArray;
    let setFlavourArray = async (key) => {
        if (!flavours.includes(key)) { 
            setFlavoursData(flavours.concat(key));
            flavoursArray = flavours.concat(key);
        } else { 
            const index = flavours.indexOf(key, 0);
            if (index > -1) {
                flavours.splice(index, 1);
                setFlavoursData(flavours.concat());
                flavoursArray = flavours.concat();
            }
        }

        await filteredDataFetch();
    };

    let typesArray;
    let setTypesArray = async (key) => {
        if (!types.includes(key)) { 
            setTypesData(types.concat(key));
            typesArray = types.concat(key);
        } else { 
            const index = types.indexOf(key, 0);
            if (index > -1) {
                types.splice(index, 1);
                setTypesData(types.concat());
                typesArray = types.concat();
            }
        }

        await filteredDataFetch();
    };

    let brandsArray;
    let setBrandsArray = async (key) => {
        if (!brands.includes(key)) { 
            setBrandsData(brands.concat(key));
            brandsArray = brands.concat(key);
        } else { 
            const index = brands.indexOf(key, 0);
            if (index > -1) {
                brands.splice(index, 1);
                setBrandsData(brands.concat());
                brandsArray = brands.concat();
            }
        }

        await filteredDataFetch();
    };

    let filteredDataFetch = async () => {
        setFilteredDataLoading(true);
        flavoursArray = (flavoursArray) ? flavoursArray : flavours;
        typesArray = (typesArray) ? typesArray : types;
        brandsArray = (brandsArray) ? brandsArray : brands;

        await axios(contextData.userAccessTokenContext)
            .post(`/products/filter`, { flavours: flavoursArray, types: typesArray, brands: brandsArray })
            .then(async(res) => {
                setFilteredItems(res.data.filtered_product_list);
                setFilteredDataLoading(false);
            });
    };

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView style={styles.mainContainer} 
                    onScroll={({nativeEvent}) => {
                        if (isCloseToBottom(nativeEvent)) {
                            if(flavours.length == 0 && types.length == 0 && brands.length == 0) {
                                asyncProductsFetch();
                            }
                        }
                    }}>
                    <View style={styles.alignItemsCenter}>
                        <TopNavigator refresh={true} subpage="false" />
                        <View style={styles.card}>
                            <Image
                                style={styles.logo}
                                source={require("../../assets/logo.png")}
                            />
                        </View>
                        <View style={styles.cardNoBg}>
                            <View style={styles.selectionContainer}>
                                <TouchableOpacity style={styles.selectAllBtn}
                                    onPress={async () => { 
                                        let selectAll = [];
                                        if(types.length == productTypeDefaultArray.length) {
                                            setTypesData(selectAll);
                                        } else {
                                            productTypeDefaultArray.map(async (item, index) => {
                                                selectAll.push(item.value);
                                            })
                                            setTypesData(selectAll);
                                            typesArray = selectAll.concat();
                                        }
                                        await filteredDataFetch();
                                    }}>
                                    {types.length != productTypeDefaultArray.length && (
                                        <Text style={styles.blackText}>Select All</Text>
                                    )}
                                    {types.length == productTypeDefaultArray.length && (
                                        <Text style={styles.blackText}>Unselect All</Text>
                                    )}
                                </TouchableOpacity>
                                {productTypeDefaultArray.length > 0 && (
                                    <>
                                        {
                                            productTypeDefaultArray.map((item, index) => {
                                                return (
                                                    <TouchableOpacity key={"ptp"+index} style={[styles.categoryBtn, (types.includes(item.value)) ? styles.bdrBgLimeCustom : styles.borderGrayCustom]}
                                                        onPress={async () => { 
                                                            await setTypesArray(item.value);
                                                        }}>
                                                        <Text adjustsFontSizeToFit={true} numberOfLines={2} style={[styles.paddingX5, (types.includes(item.value)) ? styles.limeBoldCenterText : styles.blackCenterText]}>{item.name}</Text>
                                                    </TouchableOpacity>
                                                )
                                            })
                                        }
                                    </>
                                )}
                            </View>
                        </View>
                        <View style={styles.tabContainer}>
                            <TouchableOpacity style={[styles.categoryTabs, styles.alignSelfFlexStart, (brandsTab) ? styles.bgBlack : styles.bgWhite]} onPress={() => { setFlavoursTab(false); setBrandsTab(true); }}>
                                <Text style={[(brandsTab) ?  styles.whiteText : styles.blackBoldText]}>Brands</Text>
                                <MaterialCommunityIcons
                                    name={(brandsTab) ? "menu-down" : "menu-up"}
                                    color={(brandsTab) ? "white" : "black"}
                                    size={25}
                                /> 
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.categoryTabs, (flavoursTab) ? styles.bgBlack : styles.bgWhite]} onPress={() => { setFlavoursTab(true); setBrandsTab(false); }}>
                                <Text style={[(flavoursTab) ? styles.whiteText : styles.blackBoldText]}>Flavours</Text>
                                <MaterialCommunityIcons
                                    name={(flavoursTab) ? "menu-down" : "menu-up"}
                                    color={(flavoursTab) ? "white" : "black"}
                                    size={25}
                                /> 
                            </TouchableOpacity>
                        </View>
                        <View style={[styles.card, styles.cardCustom, (flavoursTab) ? styles.displayFlex : styles.displayNone]}>
                            <View style={styles.selectionContainer}>
                                <TouchableOpacity style={[styles.selectAllBtn, styles.borderLimeCustom]}
                                    onPress={async () => { 
                                        let selectAll = [];
                                        if(flavours.length == flavoursDefaultArray.length) {
                                            setFlavoursData(selectAll);
                                        } else {
                                            flavoursDefaultArray.map(async (item, index) => {
                                                selectAll.push(item.value);
                                            })
                                            setFlavoursData(selectAll);
                                            flavoursArray = selectAll.concat();
                                        }
                                        await filteredDataFetch();
                                    }}>
                                    {flavours.length != flavoursDefaultArray.length && (
                                        <Text style={styles.blackText}>Select All</Text>
                                    )}
                                    {flavours.length == flavoursDefaultArray.length && (
                                        <Text style={styles.blackText}>Unselect All</Text>
                                    )}
                                </TouchableOpacity>
                                {flavoursDefaultArray.length > 0 && (
                                    <>
                                        {
                                            flavoursDefaultArray.map((item, index) => {
                                                return (
                                                    <TouchableOpacity key={"flv"+index} style={[styles.categoryBtn, (flavours.includes(item.value)) ? styles.bdrBgLimeCustom : styles.borderLimeCustom]}
                                                        onPress={async () => { 
                                                            await setFlavourArray(item.value);
                                                        }}>
                                                        <Text adjustsFontSizeToFit={true} numberOfLines={2} style={[styles.paddingX5, (flavours.includes(item.value)) ? styles.limeBoldCenterText : styles.blackCenterText]}>{item.name}</Text>
                                                    </TouchableOpacity>
                                                );
                                            })
                                        }
                                    </>
                                )}
                            </View>
                        </View>
                        <View style={[styles.card, styles.cardCustom, (brandsTab) ? styles.displayFlex : styles.displayNone]}>
                            <View style={styles.selectionContainer}>
                                <TouchableOpacity style={[styles.selectAllBtn, styles.borderLimeCustom]}
                                    onPress={async () => { 
                                        let selectAll = [];
                                        if(brands.length == brandsDefaultArray.length) {
                                            setBrandsData(selectAll);
                                        } else {
                                            brandsDefaultArray.map(async (item, index) => {
                                                selectAll.push(item.manufacturer_name);
                                            })
                                            setBrandsData(selectAll);
                                            brandsArray = selectAll.concat();
                                        }
                                        await filteredDataFetch();
                                    }}>
                                    {brands.length != brandsDefaultArray.length && (
                                        <Text style={styles.blackText}>Select All</Text>
                                    )}
                                    {brands.length == brandsDefaultArray.length && (
                                        <Text style={styles.blackText}>Unselect All</Text>
                                    )}
                                </TouchableOpacity>
                                {brandsDefaultArray.length > 0 && (
                                    <>
                                        {
                                            brandsDefaultArray.map((item, index) => {
                                                return (
                                                    <TouchableOpacity key={"brd"+item.id} style={[styles.categoryBtn, (brands.includes(item.manufacturer_name)) ? styles.bdrBgLimeCustom : styles.borderLimeCustom]}
                                                        onPress={async () => { 
                                                            await setBrandsArray(item.manufacturer_name);
                                                        }}>
                                                        <Text adjustsFontSizeToFit={true} numberOfLines={2} style={[styles.paddingX5, (brands.includes(item.manufacturer_name)) ? styles.limeBoldCenterText : styles.blackCenterText]}>{item.manufacturer_name}</Text>
                                                    </TouchableOpacity>
                                                );
                                            })
                                        }
                                    </>
                                )}
                            </View>
                        </View>
                        <View style={[styles.card, styles.marginBottom20]}>
                            <View style={[styles.flxDrtnRow, styles.justifyContentSpaceBetween]}>
                                <Text style={styles.cardHeadline}>Products</Text>
                                <Text style={styles.cardHeadline}>
                                    {/* <TouchableOpacity style={styles.favoritesBtn}>
                                        <MaterialCommunityIcons
                                            style={{ marginRight: 2 }}
                                            name={"heart"}
                                            color={"#C9EA3B"}
                                            size={14}
                                        /> 
                                        <Text>Favorites</Text>
                                    </TouchableOpacity> */}
                                </Text>
                            </View>
                            {(flavours.length != 0 || types.length != 0 || brands.length != 0) && filteredDataLoading && (
                                <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                            )}
                            {(flavours.length == 0 && types.length == 0 && brands.length == 0) && filteredDataLoading == false && (
                                <>
                                    {listItems.length > 0 && (
                                        <View style={[styles.flxDrtnRow, styles.flxWrap]}>
                                        {
                                            listItems.map((item, index) => {
                                                return (
                                                    <View key={"prd"+item.id} style={styles.productContainer}>
                                                        <TouchableOpacity onPress={() => productDetailScreen(item)}>
                                                            <View style={styles.productInfo}>
                                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.textBold, styles.marginRight5]}>{item.price_in_points}</Text>
                                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.textBold, styles.fontSize11]}>{item.price_in_aud} AUD</Text>
                                                            </View>
                                                            <Image source={(item.media.length > 0) ? { uri: item.media[0].path } : require("../../assets/placeholder-supplement.jpg")} style={styles.productImage}/>
                                                            <Text style={styles.productName}>
                                                                {item.name}
                                                            </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                );
                                            })
                                        }
                                        </View>
                                    )}
                                    {listItems.length <= 0 && (
                                        <Text>No available product.</Text>
                                    )}
                                </>
                            )}
                            {(flavours.length != 0 || types.length != 0 || brands.length != 0) && filteredDataLoading == false && (
                                <>
                                    {filteredItems.length > 0 && (
                                        <View style={[styles.flxDrtnRow, styles.flxWrap]}>
                                        {
                                            filteredItems.map((item, index) => {
                                                return (
                                                    <View key={"flr"+item.id} style={styles.productContainer}>
                                                        <TouchableOpacity onPress={() => productDetailScreen(item)}>
                                                            <View style={styles.productInfo}>
                                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.textBold, styles.marginRight5]}>{item.price_in_points}</Text>
                                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.textBold, styles.fontSize11]}>{item.price_in_aud} AUD</Text>
                                                            </View>
                                                            <Image source={(item.media.length > 0) ? { uri: item.media[0].path } : require("../../assets/placeholder-supplement.jpg")} style={styles.productImage}/>
                                                            <Text style={styles.productName}>
                                                                {item.name}
                                                            </Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                );
                                            })
                                        }
                                        </View>
                                    )}
                                    {filteredItems.length <= 0 && (
                                        <Text>No available product.</Text>
                                    )}
                                </>
                            )}
                            {!noMoreVerticalData && flavours.length == 0 && types.length == 0 && brands.length == 0 && (
                                <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                            )}
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default ProductScreen;