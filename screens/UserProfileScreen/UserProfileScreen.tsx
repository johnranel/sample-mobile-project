import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Alert,
    ActivityIndicator,
    Modal
} from "react-native";

import { Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useFocusEffect } from '@react-navigation/native';

import AnimatedLoader from "react-native-animated-loader";
import TopNavigator from "screens/TopNavigator/TopNavigator";

import styles from "./UserProfileScreenStyles.styles";

import axios from "services/api";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AppContext } from "../../context/AppContext";

import { StripeProvider, useStripe } from "@stripe/stripe-react-native";

const UserProfileScreen = ({ navigation }) => {
    const [fullName, setFullName] = React.useState("");
    const [profilePicture, setProfilePicture] = React.useState("");
    const [userId, setUserId] = React.useState("");
    const [listItems, setListItems] = React.useState([]);

    const [userPoints, setUserPoints] = React.useState(0);

    const [offsetTransactionData, setOffsetTransactionData] = React.useState(0);
    const [dataLoading, setDataLoading] = React.useState(false);
    const [lockVerticalLoading, setLockVerticalLoading] = React.useState(false);

    const [modalVisibility, setModalVisibility] = React.useState(false);
    const [visibleAnimatedLoader, setVisibleAnimatedLoader] = React.useState(false);
    const [paymentStatus, setPaymentStatus] = React.useState(true);

    const contextData = React.useContext(AppContext);
    const stripe = useStripe();

    let editProfileScreen = () => {
        navigation.navigate("EditProfileScreen");
    };

    let billingInformationScreen = () => {
        navigation.navigate("BillingInformationScreen");
    };

    let transactionsScreen = () => {
        navigation.navigate("TransactionsScreen");
    };

    let productDetailScreen = (item) => {
        navigation.navigate("ProductDetailScreen", item);
    };

    let topUpViaStripe = async (stripe_amount, amount_aud) => {
        setModalVisibility(true);

        let response = await axios(contextData.userAccessTokenContext)
                .post("/pay", { amount: stripe_amount })
                .then(async (res) => {
                    return res.data;
                })
                .catch((err) => {
                    return animationPaymentStatus(false);
                });

        const clientSecret = response.client_secret;
        const customerId = response.customer_id;

        const initSheet = await stripe.initPaymentSheet({ customerId: customerId, paymentIntentClientSecret: clientSecret, merchantDisplayName: 'Supplement Station' });
        if(initSheet.error) {
            return setModalVisibility(false);
        }

        const presenSheet = await stripe.presentPaymentSheet();
        if(presenSheet.error) {
            return setModalVisibility(false);
        }

        await axios(contextData.userAccessTokenContext)
            .post("/save/transaction", { type: 'top-up', amount: amount_aud, description: fullName + ' has topped up ' + amount_aud + ' AUD.', stripe_client_secret: clientSecret })
            .then(async (res) => {
                setUserPoints(res.data.points_data);
                let userPointsDataString = JSON.stringify(res.data.points_data);
                await AsyncStorage.setItem("user_points", userPointsDataString);
                animationPaymentStatus(true);
            })
            .catch(() => {
                return animationPaymentStatus(false);
            });
    }

    let animationPaymentStatus = async (status) => {
        setVisibleAnimatedLoader(true);
        setPaymentStatus(status);
        setTimeout(() => {
            setModalVisibility(false);
            setVisibleAnimatedLoader(false);
        }, 2000);
    };

    useFocusEffect(
        React.useCallback(() => {
            userAsyncDataFetch();
        }, [])
    );

    let userAsyncDataFetch = async () => {
        let userParsedData = await AsyncStorage.getItem("user");
        let user_points = await AsyncStorage.getItem("user_points");
        let user_data = JSON.parse(userParsedData);
        let user_points_data = JSON.parse(user_points);
        setUserId(user_data.id);
        setFullName(user_data.first_name + " " + user_data.last_name);
        setProfilePicture(user_data.image);
        setUserPoints(user_points_data);

        purchaseDataFetch(user_data.id);
    };

    let purchaseDataFetch = async (user_id) => {
        setDataLoading(true);
        setLockVerticalLoading(true);

        await axios(contextData.userAccessTokenContext)
            .get(`/user/transaction/get/limit/${offsetTransactionData}/type/purchase`)
            .then(async (res) => {
                setDataLoading(false);
                setLockVerticalLoading(false);
                (listItems.length != 0) ? setListItems(listItems.concat(res.data.transaction_data)) : setListItems(res.data.transaction_data);
                setOffsetTransactionData(offsetTransactionData+10);
                if (res.data.transaction_data.length !== 10) {
                    setLockVerticalLoading(true);
                }
            });
    };

    let logout = async () => {
        await AsyncStorage.removeItem("user");
        await AsyncStorage.removeItem("accessToken");
        await AsyncStorage.removeItem("user_points");
        axios(contextData.userAccessTokenContext).post(`/logout`);
        await contextData.setUserAccessTokenContext(null);
        await navigation.replace("LoginScreen");
    };

    let isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        if(!lockVerticalLoading) {
            let paddingToBottom = 10;
            return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
        } else {
            return false;
        }
    };

    return (
        <>
            <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
                <SafeAreaView style={styles.mainContainer}>
                    <ScrollView style={styles.mainContainer}
                        onScroll={({nativeEvent}) => {
                            if (isCloseToBottom(nativeEvent)) {
                                purchaseDataFetch(userId);
                            }
                        }}>
                        <View style={styles.alignItemsCenter}>
                            <TopNavigator refresh={true} subpage="false" />
                            <View style={styles.cardNoBg}>
                                <View style={[styles.accountBalance, styles.accountBalanceCustomIcon]}>
                                    {profilePicture == "" && (
                                        <MaterialCommunityIcons
                                            name={"account"}
                                            color={"white"}
                                            size={120}
                                        />
                                    )}
                                    {profilePicture != "" && (
                                        <Image source={(profilePicture) ? { uri: profilePicture } : require("../../assets/black-pic.png")} style={styles.accountBalance} />
                                    )}
                                </View>
                            </View>
                            <View style={styles.cardNoBg}>
                                <Text style={[styles.cardHeadline]}>{fullName}'s Profile</Text>
                                <View style={[styles.flxDrtnRow, styles.marginBottom5, styles.alignItemsCenter]}>
                                    <TouchableOpacity style={[styles.navButtons]} onPress={editProfileScreen}>
                                        <Text style={styles.limeBoldText}>Edit Profile</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.navButtons, styles.marginLeft5]} onPress={billingInformationScreen}>
                                        <Text style={styles.limeBoldText}>Billing Information</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={[styles.flxDrtnRow, styles.marginBottom5, styles.alignItemsCenter]}>
                                    <TouchableOpacity style={[styles.navButtons]} onPress={transactionsScreen}>
                                        <Text style={styles.limeBoldText}>Transactions</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={[styles.navButtons, styles.marginLeft5]} onPress={logout}>
                                        <Text style={styles.limeBoldText}>Logout</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ flexDirection: "row" }}>
                                <View style={styles.balanceHeadlineContainer}>
                                    <Text style={styles.cardHeadline}>Account Balance</Text>
                                </View>
                                <View style={styles.balanceContainer}>
                                    <View style={styles.accountBalance}>
                                        <Text style={[styles.limeBoldText, styles.fontSize20]}>Points</Text>
                                        <Text style={[styles.limeBoldText, styles.fontSize40]}>{(userPoints != null) ? userPoints.points : 0}</Text>
                                        <Text style={[styles.limeBoldText, styles.fontSize20]}>{(userPoints != null) ? userPoints.amount : 0} AUD</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.cardNoBg}>
                                <Text style={styles.cardHeadline}>Top-ups</Text>
                            </View>
                            <View style={styles.flxDrtnRow}>
                                <TouchableOpacity style={[styles.boxCard, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={() => topUpViaStripe(30*100, 30)}>
                                    <Text style={[styles.limeBoldText, styles.fontSize20]}>30</Text>
                                    <Text style={styles.limeBoldText}>AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.boxCard, styles.marginLeft12, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={() => topUpViaStripe(50*100, 50)}>
                                    <Text style={[styles.limeBoldText, styles.fontSize20]}>50</Text>
                                    <Text style={styles.limeBoldText}>AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.boxCard, styles.marginLeft12, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={() => topUpViaStripe(100*100, 100)}>
                                    <Text style={[styles.limeBoldText, styles.fontSize20]}>100</Text>
                                    <Text style={styles.limeBoldText}>AUD</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={[styles.boxCard, styles.marginLeft12, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={() => topUpViaStripe(150*100, 150)}>
                                    <Text style={[styles.limeBoldText, styles.fontSize20]}>150</Text>
                                    <Text style={styles.limeBoldText}>AUD</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={[styles.card, styles.marginBottom20]}>
                                <View style={[styles.flxDrtnRow, styles.justifyContentSpaceBetween]}>
                                    <Text style={styles.cardHeadline}>Last Transactions</Text>
                                    <Text style={styles.cardHeadline}>
                                        {/* <TouchableOpacity style={styles.favoritesBtn}>
                                            <MaterialCommunityIcons
                                                style={{ marginRight: 2 }}
                                                name={"heart"}
                                                color={"#C9EA3B"}
                                                size={14}
                                            />
                                            <Text>Favorites</Text>
                                        </TouchableOpacity> */}
                                    </Text>
                                </View>
                                {listItems.length > 0 && (
                                    <View style={[styles.flxDrtnRow, styles.flxWrap]}>
                                    {
                                        listItems.map((item, index) => {
                                            return (
                                                <View key={"trn"+item.id}>
                                                    <TouchableOpacity style={{flexDirection: "row", padding: 5 }} onPress={() => productDetailScreen(item.product)}>
                                                        <View>
                                                            <Image 
                                                                style={{ height: 100, width: 100, alignSelf: "center", borderRadius: 20, }}
                                                                source={(item.product.media.length > 0) ? { uri: item.product.media[0].path } : require("../../assets/placeholder-supplement.jpg")}
                                                            />
                                                        </View>
                                                        <View style={{ width: "60%", alignItems: "center", justifyContent: "center" }}>
                                                            <Text style={{ fontWeight: "bold" }}>{item.product.name}</Text>
                                                        </View>
                                                        <View style={{ width: "10%", alignItems: "center", justifyContent: "center" }}>
                                                            <Text style={{ fontWeight: "bold" }}>{item.product.price_in_points}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            );
                                        })
                                    }
                                    </View>
                                )}
                                {dataLoading && (
                                    <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                                )}
                                {listItems.length <= 0 && dataLoading == false && (
                                    <Text>No available product.</Text>
                                )}
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
                <StripeProvider publishableKey="pk_test_UE5jihFopoDx5Ka14j3VrVyW"></StripeProvider>
            </LinearGradient>
            <Modal animationType="slide"
                transparent={true}
                visible={modalVisibility}
                >
                    <View style={{ display: "flex", flex: 1, backgroundColor: "black", opacity: 0.6, alignItems: "center", alignContent: "center", justifyContent: "center" }}>
                        {!visibleAnimatedLoader && (
                            <>
                                <ActivityIndicator size="large" color="#C9EA3B" />
                                <Text style={{ color: "#C9EA3B" }}>Loading...</Text>
                            </>
                        )}
                        {visibleAnimatedLoader && (
                            <AnimatedLoader
                                visible={true}
                                overlayColor="none"
                                animationStyle={{ width: 400, height: 400, marginTop: 10 }}
                                source={(paymentStatus) ? require("../../assets/payment-successful.json") : require("../../assets/error-failure.json")}
                                speed={0.5}
                                loop={false}
                            ></AnimatedLoader>
                        )}
                    </View>
            </Modal>
        </>
    );
};

export default UserProfileScreen;