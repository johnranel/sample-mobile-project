import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
} from "react-native";

import { Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';

import TopNavigator from "screens/TopNavigator/TopNavigator";

import styles from "./PrivacyPolicyScreenStyles.styles";

const PrivacyPolicyScreen = () => {
    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView style={styles.mainContainer}>
                    <View style={styles.alignItemsCenter}>
                        <TopNavigator refresh={true} subpage="true" />
                        <View style={[styles.card, styles.marginBottom20]}>
                            <Text style={styles.cardHeadline}>
                                Privacy Policy
                            </Text>
                            <Text style={styles.cardDescription}>
                                Phasellus ut augue vitae dolor ornare tempor. Pellentesque porttitor tempor arcu, pulvinar euismod velit volutpat eu. Sed posuere malesuada eros, eget dictum lacus semper mollis. In in turpis ut odio fermentum sagittis eget sed eros. Quisque vestibulum id orci et blandit.
                                {'\n'}{'\n'}
                                In id lobortis nisl. Nunc malesuada ante vel enim dictum, ut finibus felis bibendum. Donec pellentesque pretium magna, eu ullamcorper justo fermentum nec.
                            </Text>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default PrivacyPolicyScreen;