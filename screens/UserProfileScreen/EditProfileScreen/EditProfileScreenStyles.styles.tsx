import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    gradientContainer: {
        flex: 1,
    },

    mainContainer: {
        flex: 1, 
        alignItems: "center"
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    textInput: {
        backgroundColor: "transparent",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
        color: "black",
        borderColor: "#36b9cc",
        borderWidth: 1.5,
        marginBottom: 5,
        height: 48,
    },

    textInputCustom: {
        marginLeft: 15, 
        height: 40, 
        width: "50%", 
        backgroundColor: "#36b9cc", 
        textAlign: "center"
    },

    transparentBtn: {
        marginTop: 20,
        marginBottom: 20,
        height: 40,
        width: "80%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#13181b",
        borderRadius: 25,
        borderColor: "#36b9cc",
        borderWidth: 1.5,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    whiteText: {
        color: "#36b9cc",
        fontSize: 14,
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },

    headerMsg: {
        fontSize: 32,
        textAlign: "center",
        marginBottom: 20,
    },

    imagePickerContainer: {
        height: 120, 
        width: 120, 
        backgroundColor: "#ebebeb", 
        borderRadius: 120,
        alignSelf: "center"
    },

    imagePicker: {
        height: 120, 
        width: 120, 
        borderRadius: 120
    },

    errorMessage: {
        color: "red", 
        fontSize: 14, 
        marginBottom: 10
    },

    inputContainer: {
        flexDirection: "row", 
        marginBottom: 5, 
        height: 40, 
        alignItems: "center"
    },

    dateButton: {
        height: 40, 
        marginLeft: 15, 
        width: "50%", 
        backgroundColor: "#36b9cc", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    selectionContainer: {
        flexDirection: "row", 
        marginBottom: 5, 
        height: 40, 
        alignItems: "center"
    },

    selectionContainerCustom: {
        backgroundColor: "#FBFBFB", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#36b9cc"
    },

    tabSelection: {
        height: 40, 
        width: "30%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25,
    },

    selectionContainer2: {
        flexDirection: "column", 
        marginBottom: 5, 
        alignItems: "center"
    },

    tabSelection2: {
        flexDirection: "row",
        height: 40, 
        width: "100%", 
        alignItems: "center", 
        borderRadius: 25,
    },

    tabSelectionLimeText: {
        color: "#36b9cc", 
        width: "50%", 
        textAlign: "left", 
        paddingLeft: 25
    },

    tabSelectionBlackText: {
        color: "black", 
        width: "50%", 
        textAlign: "left", 
        paddingLeft: 25
    },

    blackLimeOutlineBtn: {
        marginTop: 20,
        marginBottom: 20,
        height: 40,
        width: "90%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#13181b",
        borderRadius: 25,
        borderColor: "#36b9cc",
        borderWidth: 1.5,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    blackText: {
        color: "black",
        fontSize: 14,
    },

    limeText: {
        color: "#36b9cc",
        fontSize: 14,
    },

    grayText: {
        color: "#676767",
        fontSize: 14,
    },

    tabTextBold: {
        color: "#36b9cc", 
        fontWeight: "bold"
    },

    tabText: {
        color: "black", 
        fontSize: 14,
    },

    franchiseImageContainer: {
        width: "50%", 
        alignSelf: "center", 
        justifyContent: "center"
    },

    franchiseImage: {
        width: "100%", 
        height: 40
    },

    bgBlack: {
        backgroundColor: "black",
    },

    bgLime: {
        backgroundColor: "#36b9cc",
    },

    bgTransparent: {
        backgroundColor: "transparent",
    },

    flxDrtnColumn:{
        flexDirection: "column"
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    zIndex2: {
        zIndex: 2
    },

    width40Percent: {
        width: "40%",
    },

    width50Percent: {
        width: "50%",
    },

    width60Percent: {
        width: "60%",
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    marginTop10: {
        marginTop: 10
    },

    marginBottom0: {
        marginBottom: 0
    },

    marginBottom5: {
        marginBottom: 5
    },

    marginLeft10: {
        marginLeft: 10
    },

    marginLeft15: {
        marginLeft: 15
    },

    paddingLeft25: {
        paddingLeft: 25
    },

    alignContentCenter: {
        alignContent: "center"
    },

    justifyContentCenter: {
        justifyContent: "center"
    },

    paddingX5: {
        paddingLeft: 5,
        paddingRight: 5
    },
});