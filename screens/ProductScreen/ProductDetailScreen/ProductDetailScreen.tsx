import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
    Image,
    Modal,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";

import { Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import Swiper from 'react-native-swiper';
import { useFocusEffect } from '@react-navigation/native';
import ImageViewer from 'react-native-image-zoom-viewer';

import TopNavigator from "screens/TopNavigator/TopNavigator";

import styles from "./ProductDetailScreenStyles.styles";

import axios from "services/api";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AppContext } from "../../../context/AppContext";

const ProductDetailScreen = ({ route, navigation }) => {
    const [manufacturerName, setManufacturerName] = React.useState(route.params?.manufacturer_name);
    const [productName, setProductName] = React.useState(route.params?.name);
    const [productDescription, setProductDescription] = React.useState(route.params?.description);
    const [productPricePoints, setProductPricePoints] = React.useState(route.params?.price_in_points);
    const [productPriceAud, setProductPriceAud] = React.useState(route.params?.price_in_aud);
    const [productImageArray, setProductImageArray] = React.useState(route.params?.media);
    const [productDispenserArray, setProductDispenserArray] = React.useState(route.params?.dispenser_product_list);

    const [listItems, setListItems] = React.useState([]);
    const [filteredItems, setFilteredItems] = React.useState([]);

    const [lockVerticalLoading, setLockVerticalLoading] = React.useState(false);
    const [noMoreVerticalData, setNoMoreVerticalData] = React.useState(false);
    const [filteredDataLoading, setFilteredDataLoading] = React.useState(false);

    const [offsetProductsData, setOffsetProductsData] = React.useState(0);

    const [flavours, setFlavoursData] = React.useState([]);
    const [flavoursDefaultArray, setFlavoursDefaultArray] = React.useState([]);

    const [imageModal, setImageModal] = React.useState(false);
    const [imageViewerUrls, setImageViewerUrls] = React.useState([]);

    const [gymDispenser, setGymDispenser] = React.useState([]);

    const contextData = React.useContext(AppContext);
    
    useFocusEffect(
        React.useCallback(() => {
            asyncDataFetch();
            asyncProductsFetch();
            asyncProductTypesAndFlavoursFetch();
        }, [])
    );

    let asyncDataFetch = async () => {
        let userParsedData = await AsyncStorage.getItem("user");
        let user_data = JSON.parse(userParsedData);
        setGymDispenser(user_data.gym.dispensers);
    };

    let asyncProductsFetch = async () => {
        setLockVerticalLoading(true);
        await axios(contextData.userAccessTokenContext)
            .get(`/products/get/${offsetProductsData}`)
            .then(async(res) => {
                (listItems.length != 0) ? setListItems(listItems.concat(res.data.product_list)) : setListItems(res.data.product_list);
                setOffsetProductsData(offsetProductsData+6);
                setLockVerticalLoading(false);
                if (res.data.product_list.length !== 6) {
                    setNoMoreVerticalData(true);
                }
            });
    };

    let asyncProductTypesAndFlavoursFetch = async () => {
        await axios(contextData.userAccessTokenContext)
            .get(`/product/brand-types-and-flavours/get`)
            .then(async(res) => {
                setFlavoursDefaultArray(res.data.product_flavours);
            });
    };

    let isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        if(!lockVerticalLoading) {
            let paddingToBottom = 10;
            return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
        } else {
            return false;
        }
    };

    let flavoursArray;
    let setFlavourArray = async (key) => {
        if (!flavours.includes(key)) { 
            setFlavoursData(flavours.concat(key));
            flavoursArray = flavours.concat(key);
        } else { 
            const index = flavours.indexOf(key, 0);
            if (index > -1) {
                flavours.splice(index, 1);
                setFlavoursData(flavours.concat());
                flavoursArray = flavours.concat();
            }
        } 
       
        await filteredDataFetch();
    };

    let filteredDataFetch = async () => {
        setFilteredDataLoading(true);
        await axios(contextData.userAccessTokenContext)
            .post(`/products/filter`, { flavours: flavoursArray })
            .then(async(res) => {
                setFilteredItems(res.data.filtered_product_list);
                setFilteredDataLoading(false);
            });
    };

    useFocusEffect(
        React.useCallback(() => {
            setManufacturerName(route.params?.manufacturer_name);
            setProductName(route.params?.name);
            setProductDescription(route.params?.description);
            setProductPricePoints(route.params?.price_in_points);
            setProductPriceAud(route.params?.price_in_aud);
            setProductImageArray(route.params?.media);
            setProductDispenserArray(route.params?.dispenser_product_list);
        }, [route])
    );

    const productDetailScreen = (item) => {
        navigation.navigate("ProductDetailScreen", item);
    };

    const imageFocus = async (imagePath) => {
        setImageModal(true);
        setImageViewerUrls([{ url: imagePath }]);
    };

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView style={styles.mainContainer}
                    onScroll={({nativeEvent}) => {
                        if (isCloseToBottom(nativeEvent)) {
                            if(flavours.length == 0) {
                                asyncProductsFetch();
                            }
                        }
                    }}>
                    <View style={styles.alignItemsCenter}>
                        <TopNavigator refresh={true} subpage="true" />
                        <View style={styles.card}>
                            {productImageArray.length == 0 && (
                                <Swiper style={styles.wrapper}>
                                    <TouchableOpacity>
                                        <Image 
                                            style={styles.productImage}
                                            source={require("../../../assets/placeholder-supplement.jpg")}
                                        />
                                    </TouchableOpacity>
                                </Swiper>
                            )}
                            {productImageArray.length > 0 && (
                                <Swiper style={styles.wrapper}>
                                    {productImageArray.map((image) => {
                                        if(image.type == "image") {
                                            return (
                                                <TouchableOpacity key={"img"+image.id} onPress={() => { imageFocus(image.path)}}>
                                                    <Image 
                                                        style={styles.productImage}
                                                        source={{uri: image.path}}
                                                    />
                                                </TouchableOpacity>
                                            )
                                        }
                                    })}
                                    {productImageArray.map((image) => {
                                        if(image.type == "nutrients") {
                                            return (
                                                <TouchableOpacity key={"nts"+image.id} onPress={() => { imageFocus(image.path)}}>
                                                    <Image 
                                                        style={styles.productImage}
                                                        source={{uri: image.path}}
                                                    />
                                                </TouchableOpacity>
                                            )
                                        }
                                    })}
                                </Swiper>
                            )}
                            <Text style={styles.cardSubHeadline}>{manufacturerName}</Text>
                            <View style={styles.flxDrtnRow}>
                                <View style={styles.width70Percent}>
                                    <Text style={[styles.cardHeadline]}>
                                        {productName}
                                        <MaterialCommunityIcons
                                            style={styles.paddingRight5}
                                            name={"star"}
                                            color={"black"}
                                            size={30}
                                        />
                                    </Text>
                                </View>
                                <View style={styles.productPricingContainer}>
                                    <Text style={[styles.textAlignCenter, styles.fontSize20, styles.textBold]}>
                                        {productPricePoints}
                                    </Text>
                                    <Text style={[styles.textAlignCenter, styles.fontSize15, styles.textBold, styles.marginLeft10]}>
                                        {productPriceAud} AUD
                                    </Text>
                                </View>
                            </View>
                            <Text style={[styles.cardDescription, styles.textBold, styles.marginBottom10]}>
                                Available in Dispenser
                                {
                                    productDispenserArray.map((dispensers, indexProductDispenser) => {
                                        let result = gymDispenser.map((gymDispensers) => {
                                            if(dispensers.dispensers_id === gymDispensers.dispensers_id) {
                                                if(indexProductDispenser === productDispenserArray.length - 1) {
                                                    return " " + dispensers.dispensers_id;
                                                } else {
                                                    return " " + dispensers.dispensers_id + ",";
                                                }
                                            }
                                        })
                                        
                                        return result;
                                    })
                                }
                            </Text>
                            <Text style={[styles.cardDescription, styles.flxWrap, styles.lineHeight18, styles.textAlignLeft]}>
                                {productDescription}
                            </Text>
                        </View>
                        <View style={[styles.cardNoBg, styles.flxDrtnRow, styles.alignItemsCenter, styles.flxWrap]}>
                            <Text style={[styles.cardSubHeadline, styles.marginBottom0, styles.marginRight10, styles.blackText]}>Other flavours</Text>
                            {
                                flavoursDefaultArray.map((item, index) => {
                                    return (
                                        <TouchableOpacity key={"flv"+index} style={[styles.flavoursSelection, (flavours.includes(item.value)) ? styles.bgBlack : styles.bgLime]} 
                                            onPress={async () => { 
                                                await setFlavourArray(item.value);
                                            }}>
                                            <Text style={[(flavours.includes(item.value)) ? styles.limeBoldCenterText : styles.blackCenterText]}>
                                                {item.name}
                                            </Text>
                                        </TouchableOpacity>
                                    );
                                })
                            }
                        </View>
                        <View style={[styles.card, styles.marginBottom20]}>
                            <View style={[styles.flxDrtnRow, styles.justifyContentSpaceBetween]}>
                                <Text style={styles.cardSubHeadline}>Products</Text>
                                <View style={styles.cardHeadline}>
                                    {/* <TouchableOpacity style={styles.favoritesBtn}>
                                        <MaterialCommunityIcons
                                            style={styles.marginRight2}
                                            name={"heart"}
                                            color={"#C9EA3B"}
                                            size={14}
                                        /> 
                                        <Text>Favorites</Text>
                                    </TouchableOpacity> */}
                                </View>
                            </View>
                            {flavours.length == 0 && (
                                <>
                                    {listItems.length > 0 && (
                                        <View style={[styles.flxDrtnRow, styles.flxWrap]}>
                                            {
                                                listItems.map((item, index) => {
                                                    return (
                                                        <View key={"prd"+item.id} style={styles.productContainer}>
                                                            <TouchableOpacity onPress={() => productDetailScreen(item)}>
                                                                <View style={styles.productInfo}>
                                                                    <Text style={[styles.textBold, styles.marginRight5]}>{item.price_in_points}</Text>
                                                                    <Text style={[styles.textBold, styles.fontSize11]}>{item.price_in_aud} AUD</Text>
                                                                </View>
                                                                <Image source={(item.media.length > 0) ? { uri: item.media[0].path } : require("../../../assets/placeholder-supplement.jpg")} style={{ height: 100, width: "100%" }}/>
                                                                <Text style={styles.productName}>
                                                                    {item.name}
                                                                </Text>
                                                            </TouchableOpacity>
                                                        </View>
                                                    );
                                                })
                                            }
                                        </View>
                                    )}
                                </>
                            )}
                            {flavours.length > 0 && (
                                <View style={[styles.flxDrtnRow, styles.flxWrap]}>
                                {
                                    filteredItems.map((item, index) => {
                                        return (
                                            <View key={"flt"+item.id} style={styles.productContainer}>
                                                <TouchableOpacity onPress={() => productDetailScreen(item)}>
                                                    <View style={styles.productInfo}>
                                                        <Text style={[styles.textBold, styles.marginRight5]}>{item.price_in_points}</Text>
                                                        <Text style={[styles.textBold, styles.fontSize11]}>{item.price_in_aud} AUD</Text>
                                                    </View>
                                                    <Image source={(item.media.length > 0) ? { uri: item.media[0].path } : require("../../../assets/placeholder-supplement.jpg")} style={{ height: 100, width: "100%" }}/>
                                                    <Text style={styles.productName}>
                                                        {item.name}
                                                    </Text>
                                                </TouchableOpacity>
                                            </View>
                                        );
                                    })
                                }
                                </View>
                            )}
                            {flavours.length != 0 && filteredDataLoading && (
                                <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                            )}
                            {(filteredItems.length <= 0 && listItems.length <= 0) || (flavours.length != 0 && filteredItems.length <= 0) && (
                                <Text>No available product.</Text>
                            )}
                            {!noMoreVerticalData && flavours.length == 0 && (
                                <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                            )}
                        </View>
                    </View>
                </ScrollView>
                <Modal visible={imageModal} transparent={true}>
                    <TouchableOpacity onPress={() => { setImageModal(false) }} style={{ position: "absolute", top: 50, right: 10, zIndex: 10 }}>
                        <MaterialCommunityIcons
                            style={styles.marginRight2}
                            name={"close-thick"}
                            color={"#C9EA3B"}
                            size={20}
                        /> 
                    </TouchableOpacity>
                    <ImageViewer imageUrls={imageViewerUrls} style={{ zIndex: 9 }}/>
                </Modal>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default ProductDetailScreen;