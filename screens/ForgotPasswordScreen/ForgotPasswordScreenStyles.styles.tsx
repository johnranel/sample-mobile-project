import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    mainContainer: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },

    card: {
        backgroundColor: "#ffffff", 
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },
    
    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    cardDescription: {
        marginTop: 10, 
        marginBottom: 20,
        fontSize: 14,
    },

    textInput: {
        backgroundColor: "#36b9cc",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
        paddingLeft: 25,
        paddingRight: 25,
        color: "black",
        borderColor: "white",
        marginBottom: 5,
        height: 48,
        width: "100%"
    },

    container: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        zIndex: 2,
    },

    fieldsContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },

    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22,
    },

    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },

    modalLoadingContainer: {
        alignSelf: "center",
    },

    modalTitle: {
        fontSize: 18, 
        paddingBottom: 5, 
        textAlign: "center",
    },

    modalMessage: { 
        textAlign: "center",
    },

    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        marginHorizontal: 5,
    },

    buttonClose: {
        marginTop: 15,
        backgroundColor: "white",
    },

    buttonCloseTextStyle: {
        color: "black",
        textAlign: "center",
    },

    blackLimeOutlineBtn: {
        height: 40,
        width: 149,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#13181b",
        borderRadius: 25,
        borderColor: "#36b9cc",
        borderWidth: 1.5,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    limeText: {
        color: "#36b9cc",
        fontSize: 14,
    },

    errorMessage: {
        color: "red",
        fontSize: 14,
        alignSelf: "center",
    },

    textDecorationUnderline: {
        textDecorationLine: "underline",
    },

    marginTop15: {
        marginTop: 15,
    },

    marginTop10: {
        marginTop: 10,
    },
});