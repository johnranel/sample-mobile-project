import React from "react";
import { 
    Text, 
    View, 
    TouchableOpacity,
    Modal,
    ActivityIndicator,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Platform
} from "react-native";

import { Button, TextInput } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';

import styles from "./ForgotPasswordScreenStyles.styles";
import axios from "services/api";

const ForgotPasswordScreen = ({ route, navigation }) => {
    const [email, setEmail] = React.useState("");
    const [error_email, setErrorEmail] = React.useState("");
    const [loading, setLoading] = React.useState(false);
    const [showModal, setShowModal] = React.useState(false);
    const [requestStatus, setRequestStatus] = React.useState("");
    const [requestMessage, setRequestMessage] = React.useState("");

    let validateData = () => {
        const expression = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

        const validationResult = expression.test(String(email).toLowerCase());

        if (validationResult == false) {
            setErrorEmail("Please use a valid email.");
        } else {
            setErrorEmail("");
            forgotPasswordMail();
        }
    };

    let forgotPasswordMail = async () => {
        const data = { email: email };
        setShowModal(true);
        setLoading(true);
        let response = await axios()
            .post("/forgot-pasword", data)
            .then(async (res) => {
                setRequestStatus("Email sent.");
                setRequestMessage(res.data.status);
                setLoading(false);
            })
            .catch((err) => {
                setRequestStatus("Error occurred.");
                setRequestMessage("Email provided is not found/registered in our system.");
                setLoading(false);
            });
    };

    let previousScreen = () => {
        navigation.goBack();
    };

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={{ flex: 1 }}>
            <KeyboardAvoidingView style={styles.mainContainer} behavior={Platform.OS === "ios" ? "padding" : "height"} enabled>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.card}>
                        <View style={styles.container}>
                            <Text style={styles.cardHeadline}>
                                Forgot Password
                            </Text>
                            <Text style={styles.cardDescription}>
                                Enter your email and the password reset link will be sent to the registered email.
                            </Text>
                        </View>
                        <View style={styles.fieldsContainer}>
                            <TextInput
                                style={styles.textInput}
                                label="Email address"
                                value={email}
                                underlineColor="transparent"
                                selectionColor="black"
                                activeUnderlineColor="transparent"
                                onChangeText={(text) => setEmail(text)}
                            />
                        </View>
                        {error_email != "" && (
                            <Text style={styles.errorMessage}>{error_email}</Text>
                        )}

                        <View style={[styles.container, styles.marginTop15]}>
                            <TouchableOpacity
                                style={styles.blackLimeOutlineBtn}
                                onPress={() => validateData()}
                            >
                                <Text style={styles.limeText}>SEND</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.marginTop10}
                                onPress={previousScreen}
                            >
                                <Text style={styles.textDecorationUnderline}>Go back</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={showModal}
                    onRequestClose={() => {
                        setShowModal(false);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            {(loading &&
                            <View style={styles.modalLoadingContainer}>
                                <ActivityIndicator size="large" color="#12bcc9" />
                                <Text>Loading...</Text>
                            </View>
                            )}
                            {(!loading &&
                            <View>
                                <Text style={styles.modalTitle}>{requestStatus}</Text>
                                <Text style={styles.modalMessage}>{requestMessage}</Text>
                                <TouchableOpacity
                                    style={[styles.button, styles.buttonClose]}
                                    onPress={() => { previousScreen() }}
                                >
                                    <Text style={styles.buttonCloseTextStyle}>Close</Text>
                                </TouchableOpacity>
                            </View>
                            )}
                        </View>
                    </View>
                </Modal>
            </KeyboardAvoidingView>
        </LinearGradient>
    );
};

export default ForgotPasswordScreen;