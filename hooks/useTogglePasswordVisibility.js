import { useState } from 'react';

export const useTogglePasswordVisibility = () => {
    const [passwordVisibility, setPasswordVisibility] = useState(true);
    const [confirmPasswordVisibility, setConfirmPasswordVisibility] = useState(true);
    const [passwordRightIcon, setPasswordRightIcon] = useState('eye');
    const [confirmPasswordRightIcon, setConfirmRightIcon] = useState('eye');

    let handlePasswordVisibility = () => {
        if (passwordRightIcon === 'eye') {
            setPasswordRightIcon('eye-off');
            setPasswordVisibility(!passwordVisibility);
        } else if (passwordRightIcon === 'eye-off') {
            setPasswordRightIcon('eye');
            setPasswordVisibility(!passwordVisibility);
        }
    };

    let handleConfrimPasswordVisibility = () => {
        if (confirmPasswordRightIcon === 'eye') {
            setConfirmRightIcon('eye-off');
            setConfirmPasswordVisibility(!confirmPasswordVisibility);
        } else if (confirmPasswordRightIcon === 'eye-off') {
            setConfirmRightIcon('eye');
            setConfirmPasswordVisibility(!confirmPasswordVisibility);
        }
    };

  return {
    passwordVisibility,
    confirmPasswordVisibility,
    passwordRightIcon,
    confirmPasswordRightIcon,
    handlePasswordVisibility,
    handleConfrimPasswordVisibility
  };
};