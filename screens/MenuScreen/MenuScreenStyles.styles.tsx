import { StyleSheet, Dimensions } from "react-native";

export default StyleSheet.create({
    gradientContainer: {
        flex: 1,
        alignItems: "center"
    },

    mainContainer: {
        flex: 1, 
        width: "100%"
    },

    topBarContainer: {
        flexDirection: "row",
        alignContent: "center",
        justifyContent: "center",
    },

    topBarItems: {
        width: Dimensions.get('window').width - 25,
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },

    alignItemsCenter: { 
        alignItems: "center" 
    },

    topBarBtn: { 
        padding: 5, 
        backgroundColor: "#d9ff00", 
        width: 36, 
        borderRadius: 10 
    },

    marginRight2: {
        marginRight: 2
    },

    whiteBoldText30: {
        color: "white", 
        fontWeight: "bold",
        fontSize: 30
    }
});