import React from "react";

import {
    SafeAreaView,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    Modal,
    ActivityIndicator
} from "react-native";

import { Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import MapView, { Marker } from 'react-native-maps';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useFocusEffect } from '@react-navigation/native';
import SearchableDropdown from 'react-native-searchable-dropdown';

import TopNavigator from "screens/TopNavigator/TopNavigator";

import styles from "./GymFranchiseScreenStyles.styles";

import axios from "services/api";
import cities from "assets/au.json";

const GymFranchiseScreen = ({ navigation }) => {
    const [modalVisibility, setModalVisibility] = React.useState(false);
    const [selectedLocation, setSelectedLocation] = React.useState([]);
    const [gymFranchise, setGymFranchise] = React.useState([]);
    const [individualGym, setIndividualGym] = React.useState([]);
    const [selectedGymFranchise, setSelectedGymFranchise] = React.useState([]);
    const [selectedIndividualGym, setSelectedIndividualGym] = React.useState([]);
    const [individualGymMarkers, setIndividualGymMarkers] = React.useState([]);
    const [loading, setLoading] = React.useState(false);
    const [mapView, setMapView] = React.useState({
        latitude: -33.8650,
        longitude: 151.2094,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    });
    const map = React.useRef<MapView>(null);

    const [screenFocused, setScreenFocused] = React.useState(false);

    useFocusEffect(
        React.useCallback(() => {
            if(screenFocused === false) {
                fetchGymFranchise();
            }
            return () => {
                setScreenFocused(true);
            };
        }, [screenFocused])
    );

    let fetchGymFranchise = async () => {
        setLoading(true);
        await axios().get("/gym-franchise/get-all").then(async (res) => {
            setGymFranchise(res.data.gym_franchise);
            let gyms = [];

            res.data.gym_franchise.map(async (item, index) => {
                gyms.push(item.gyms[0]);
            });

            let i = 1;
            gyms.map(async (item, index) => {
                await fetch(`https://nominatim.openstreetmap.org/search?q=${item.address}&format=json`)
                .then(async (res) => res.json())
                .then(async (res) => {
                    let dataResponse = [{
                        id: i,
                        latitude: parseFloat(res[0].lat),
                        longitude: parseFloat(res[0].lon),
                        title: item.name,
                    }];

                    setIndividualGymMarkers((prev) => [...prev, ...dataResponse]);

                    i++;
                });
            });
            setLoading(false);
        })
        .catch((err) => {
            setLoading(false);
        });
    };

    let setLocation = (item) => {
        setSelectedLocation(item); 
        setMapView({ latitude: item.latitude, longitude: item.longitude, latitudeDelta: 0.0922, longitudeDelta: 0.0421 });
    };

    let setMapLocation = () => {
        map.current?.animateToRegion(mapView);
    };

    return (
        <>
            <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
                <SafeAreaView style={styles.mainContainer}>
                    <ScrollView style={styles.mainContainer}>
                        <View style={styles.alignItemsCenter}>
                            <TopNavigator refresh={true} subpage="false" />
                            <View style={styles.card}>
                                <Image
                                    style={styles.logo}
                                    source={require("../../assets/logo.png")}
                                />
                            </View>
                            <View style={styles.cardNoBg}>
                                <View style={styles.flxDrtnRow}>
                                    <Text style={[styles.cardHeadline, styles.marginRight20]}>Location</Text>
                                    <TouchableOpacity style={styles.selectionButton}>
                                        <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.blackText, styles.paddingX2]}>{(Object.keys(selectedLocation).length > 0) ? selectedLocation.name : 'Sydney' }</Text>
                                    </TouchableOpacity>
                                    <View style={styles.selectionContainer}>
                                        <TouchableOpacity style={styles.selectionBlackButton} onPress={() => setModalVisibility(true)}>
                                            <MaterialCommunityIcons
                                                style={styles.paddingRight2}
                                                name={"map-marker"}
                                                color={"#d9ff00"}
                                                size={14}
                                            /> 
                                            <Text style={styles.limeText}>{(Object.keys(selectedLocation).length > 0) ? selectedLocation.name : 'Sydney' }</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <View style={styles.mapContainer}>
                                    <MapView style={styles.map} 
                                        ref={map}
                                        region={{ latitude: -33.8650, longitude: 151.2094, latitudeDelta: 0.0922, longitudeDelta: 0.0421 }}
                                    >
                                        {
                                            individualGymMarkers.map((item, index) => {
                                                return (
                                                    <Marker
                                                        key={index}
                                                        coordinate={{ latitude : item.latitude , longitude : item.longitude }}
                                                        title={item.title}
                                                    />
                                                );
                                            })
                                        }
                                    </MapView>
                                </View>
                                <Text style={styles.cardHeadline}>Gym Franchise</Text>
                                {Object.keys(selectedGymFranchise).length <= 0 && (
                                    <Text style={[styles.marginVertical10, styles.errorMessage]}>Please select a franchise.</Text>
                                )}
                                <View style={styles.gymFranchise}>
                                    {loading && (
                                        <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                                    )}
                                    {gymFranchise.length > 0 && (
                                        <>
                                            {
                                                gymFranchise.map((item, index) => {
                                                    return (
                                                        <TouchableOpacity key={"frn"+item.id} style={[styles.selectionPillsBtn, (selectedGymFranchise === item) ? styles.bgBlackBorderLimeCustom : styles.borderCustom]} onPress={() => { setSelectedGymFranchise(item); setIndividualGym(item.gyms) }}>
                                                            <Text adjustsFontSizeToFit={true} numberOfLines={2} style={[(selectedGymFranchise === item) ? styles.limeBoldCenterText : styles.blackCenterText]}>{item.name}</Text>
                                                        </TouchableOpacity>
                                                    );
                                                })
                                            }
                                        </>
                                    )}
                                </View>
                                <Text style={styles.cardHeadline}>Gyms with Machines near you</Text>
                                {Object.keys(selectedIndividualGym).length <= 0 && (
                                    <Text style={[styles.marginVertical10, styles.errorMessage]}>Please select a gym.</Text>
                                )}
                                <View style={[styles.marginBottom5, styles.alignItemsCenter]}>
                                    {individualGym.length > 0 && (
                                        <>
                                            {
                                                individualGym.map((item, index) => {
                                                    return (
                                                        <TouchableOpacity key={"gym"+item.id} style={[styles.selectionTabsBtn, (selectedIndividualGym === item) ? styles.bgBlackBorderLimeCustom : styles.borderCustom]} onPress={() => { setSelectedIndividualGym(item); }}>
                                                            <Text adjustsFontSizeToFit={true} numberOfLines={2} style={[styles.textBold, (selectedIndividualGym === item) ? styles.limeText : styles.blackText]}>{item.name}</Text>
                                                        </TouchableOpacity>
                                                    );
                                                })
                                            }
                                        </>
                                    )}
                                    {Object.keys(selectedGymFranchise).length > 0 && Object.keys(individualGym).length <= 0 && (
                                        <Text style={styles.marginVertical10}>No available Gym.</Text>
                                    )}
                                </View>
                                {Object.keys(selectedIndividualGym).length > 0 && (
                                    <View style={[styles.card, { marginTop: 0, marginBottom: 20 }]}>
                                        <View style={styles.flxDrtnRow}>
                                            <Text style={[styles.cardHeadline]}>{selectedIndividualGym.name}</Text>
                                        </View>
                                        <Text style={styles.cardDescription}>
                                            {selectedIndividualGym.address}
                                        </Text>
                                        <Text style={[styles.blackBoldText, styles.marginBottom5]}>
                                            Machine Availability
                                        </Text>
                                        <Text style={[styles.blackText, styles.marginBottom10]}>
                                            Available
                                        </Text>
                                    </View>
                                )}
                                {Object.keys(selectedIndividualGym).length <= 0 && (
                                    <View style={[styles.card, styles.marginTop0, styles.marginBottom20]}>
                                        <Text style={[styles.blackBoldText, styles.marginBottom5]}>
                                            Gym Information
                                        </Text>
                                        <Text style={[styles.blackText, styles.marginBottom10]}>
                                            No available gym information.
                                        </Text>
                                    </View>
                                )}
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </LinearGradient>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisibility}
                onRequestClose={() => {
                    setModalVisibility(false);
                }}
            >
                <View style={styles.modalContainer}>
                    <View style={[styles.card, styles.marginVertical0, styles.width80Percent]}>
                        <Text style={[styles.cardHeadline, styles.marginBottom10]}>
                            <MaterialCommunityIcons
                                style={styles.paddingRight2}
                                name={"map-marker"}
                                color={"#ccc"}
                                size={14}
                            /> 
                            Select location
                        </Text>
                        <SearchableDropdown
                            selectedItems={selectedLocation}
                            onItemSelect={(item) => setLocation(item)}
                            containerStyle={{ padding: 5 }}
                            itemStyle={styles.searchDropdown}
                            itemTextStyle={styles.blackText}
                            itemsContainerStyle={{ maxHeight: 140 }}
                            items={cities}
                            defaultIndex={2}
                            resetValue={false}
                            textInputProps={
                                {
                                    placeholder: (selectedLocation.length > 0) ? selectedLocation.name : "Select location",
                                    underlineColorAndroid: "transparent",
                                    style: {
                                        padding: 12,
                                        borderWidth: 1,
                                        borderColor: '#ccc',
                                        borderRadius: 5,
                                    },
                                }
                            }
                        />
                        <View style={[styles.flxDrtnRow, styles.marginTop10]}>
                            <TouchableOpacity style={[styles.modalBtn, styles.modalLimeBtn]} onPress={() => { setMapLocation(); setModalVisibility(false); }}>
                                <Text style={styles.limeText}>SUBMIT</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.modalBtn, styles.modalBtnCstm]} onPress={() => { setModalVisibility(false); }}>
                                <Text style={styles.blackText}>CLOSE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        </>
    );
};

export default GymFranchiseScreen;