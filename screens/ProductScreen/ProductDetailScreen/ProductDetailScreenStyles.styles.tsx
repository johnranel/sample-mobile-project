import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    gradientContainer: {
        flex: 1,
        alignItems: "center"
    },

    mainContainer: {
        flex: 1, 
        width: "100%"
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 25,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    cardSubHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        color: "#999"
    },

    cardHeadline: {
        fontSize: 30,
        fontWeight: "bold",
        marginBottom: 10
    },

    cardDescription: {
        fontSize: 14,
        color: "#999"
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },

    productImage: {
        height: 250, 
        width: 250, 
        alignSelf: "center"
    },

    productPricingContainer: {
        height: 30, 
        width: "30%", 
        backgroundColor: "#36b9cc", 
        borderRadius: 20, 
        alignItems: "center", 
        justifyContent: "center", 
        flexDirection: "row"
    },

    productContainer: {
        width: (Dimensions.get('window').width / 3) - 25, 
        marginBottom: 10, 
        padding: 5 
    },

    productInfo: {
        position: "absolute", 
        right: 15, 
        zIndex: 2, 
        paddingVertical: 5, 
        paddingHorizontal: 10, 
        backgroundColor: "#36b9cc", 
        borderRadius: 20, 
        alignItems: "center", 
        flexDirection: "row"
    },

    productName: {
        textAlign: "center", 
        fontWeight: "bold", 
        color: "#676767", 
        marginTop: 5
    },

    favoritesBtn: {
        flexDirection: "row", 
        paddingVertical: 2.5, 
        paddingHorizontal: 10, 
        borderWidth: 1.5, 
        borderColor: "#36b9cc", 
        borderRadius: 15, 
        alignContent: "center", 
        justifyContent: "center",
        width: 100
    },

    flavoursSelection: {
        paddingVertical: 5, 
        paddingHorizontal: 15, 
        borderRadius: 20, 
        justifyContent: "center", 
        marginRight: 5, 
        marginBottom: 5 
    },

    limeBoldCenterText: {
        color: "#36b9cc",
        fontSize: 14,
        fontWeight: "bold",
        textAlign: "center",
    },

    blackText: {
        color: "black",
    },

    blackCenterText: {
        color: "black",
        fontSize: 14,
        textAlign: "center"
    },

    width50Percent: {
        width: "50%",
    },

    wrapper: {
        height: 300,
    },

    alignItemsCenter: {
        alignItems: "center"
    },

    alignItemsFlexEnd: {
        alignItems: "flex-end"
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    paddingHorizontal10: {
        paddingHorizontal: 10
    },

    paddingRight5: {
        paddingRight: 5
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    flxWrap: {
        flexWrap: "wrap"
    },

    width70Percent: {
        width: "70%",
    },

    width75Percent: {
        width: "75%",
    },

    textAlignLeft: {
        textAlign: "left"
    },

    textAlignCenter: {
        textAlign: "center"
    },

    lineHeight18: {
        lineHeight: 18
    },

    fontSize11: {
        fontSize: 11
    },

    fontSize15: {
        fontSize: 15
    },

    fontSize20: {
        fontSize: 20
    },

    textBold: {
        fontWeight: "bold"
    },

    marginLeft5: {
        marginRight: 5
    },

    marginRight2: {
        marginRight: 2
    },

    marginRight5: {
        marginRight: 5
    },

    marginRight10: {
        marginRight: 10
    },

    marginLeft10: {
        marginLeft: 10
    },

    marginBottom0: {
        marginBottom: 0
    },

    marginBottom10: {
        marginBottom: 10
    },

    marginBottom20: {
        marginBottom: 20
    },

    bgBlack: {
        backgroundColor: "black"
    },

    bgLime: {
        backgroundColor: "#36b9cc"
    },

    bgLimeBordered: {
        borderWidth: 1.5, 
        borderColor: "#36b9cc", 
        backgroundColor: "#36b9cc"
    },

    justifyContentSpaceBetween: {
        justifyContent: "space-between"
    }
    
});