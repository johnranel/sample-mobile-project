import React from "react";

import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import MainScreen from "screens/MainScreen/MainScreen";

import ProductScreen from "screens/ProductScreen/ProductScreen";
import ProductDetailScreen from "screens/ProductScreen/ProductDetailScreen/ProductDetailScreen";

import PrioritiesScreen from "screens/PrioritiesScreen/PrioritiesScreen";
import GymFranchiseScreen from "screens/GymFranchiseScreen/GymFranchiseScreen";

import UserProfileScreen from "screens/UserProfileScreen/UserProfileScreen";
import EditProfileScreen from "screens/UserProfileScreen/EditProfileScreen/EditProfileScreen";
import GymSelectionScreen from "screens/UserProfileScreen/EditProfileScreen/GymSelectionScreen/GymSelectionScreen";
import BillingInformationScreen from "screens/UserProfileScreen/BillingInformationScreen/BillingInformationScreen";
import TransactionsScreen from "screens/UserProfileScreen/TransactionsScreen/TransactionsScreen";

const BottomNavigator = ({ navigation }) => {
    const Tab = createBottomTabNavigator();
    const Stack = createStackNavigator();

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    let iconColor;
        
                    if (route.name === 'Home') {
                        iconName = 'home-variant';
                        iconColor = focused ? '#36b9cc' : '#ddd';
                    } else if (route.name === 'Product') {
                        iconName = 'beer';
                        iconColor = focused ? '#36b9cc' : '#ddd';
                    } else if (route.name === 'Priorities') {
                        iconName = 'dumbbell';
                        iconColor = focused ? '#36b9cc' : '#ddd';
                    } else if (route.name === 'Gym Franchise') {
                        iconName = 'map-marker';
                        iconColor = focused ? '#36b9cc' : '#ddd';
                    } else if (route.name === 'User') {
                        iconName = 'account';
                        iconColor = focused ? '#36b9cc' : '#ddd';
                    }
        
                    return <MaterialCommunityIcons name={iconName} color={iconColor} size={25}
                /> ;
                },
                tabBarShowLabel: false,
                tabBarStyle: {
                    backgroundColor: "#000"
                }
            })}>
            <Tab.Screen name="Home" options={{ headerShown: false, lazy: false }}>
                {() => (
                    <Stack.Navigator initialRouteName="MainScreen">
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="MainScreen"
                            component={MainScreen}
                        />
                    </Stack.Navigator>
                )}
            </Tab.Screen>
            <Tab.Screen name="Product" options={{ headerShown: false, lazy: false }}>
                {() => (
                    <Stack.Navigator initialRouteName="ProductScreen">
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="ProductScreen"
                            component={ProductScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="ProductDetailScreen"
                            component={ProductDetailScreen}
                        />
                    </Stack.Navigator>
                )}
            </Tab.Screen>
            <Tab.Screen name="Priorities" options={{ headerShown: false, lazy: false }}>
                {() => (
                    <Stack.Navigator initialRouteName="PrioritiesScreen">
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="PrioritiesScreen"
                            component={PrioritiesScreen}
                        />
                    </Stack.Navigator>
                )}
            </Tab.Screen>
            <Tab.Screen name="Gym Franchise" options={{ headerShown: false, lazy: false }}>
                {() => (
                    <Stack.Navigator initialRouteName="GymFranchiseScreen">
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="GymFranchiseScreen"
                            component={GymFranchiseScreen}
                        />
                    </Stack.Navigator>
                )}
            </Tab.Screen>
            <Tab.Screen name="User" options={{ headerShown: false, lazy: false }}>
                {() => (
                    <Stack.Navigator initialRouteName="UserProfileScreen">
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="UserProfileScreen"
                            component={UserProfileScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="EditProfileScreen"
                            component={EditProfileScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="GymSelectionScreen"
                            component={GymSelectionScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="BillingInformationScreen"
                            component={BillingInformationScreen}
                        />
                        <Stack.Screen
                            options={{ headerShown: false }}
                            name="TransactionsScreen"
                            component={TransactionsScreen}
                        />
                    </Stack.Navigator>
                )}
            </Tab.Screen>
        </Tab.Navigator>
    );
};

export default BottomNavigator;