import { StyleSheet, Dimensions, Platform } from "react-native";

export default StyleSheet.create({
    gradientContainer: {
        flex: 1, 
        alignItems: "center"
    },

    mainContainer: {
        flexGrow: 1, 
        width: "100%"
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    logo: {
        width: "100%", 
        resizeMode: "cover", 
        height: 120
    },

    boxCard: {
        backgroundColor: "black", 
        marginTop: 20,
        width: (Dimensions.get('window').width / 4) - 15,
        height: (Dimensions.get('window').width / 4) - 15,
        borderWidth: 1.5,
        borderColor: "#36b9cc",
        borderRadius: 20,
        padding: 10,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    balanceHeadlineContainer: {
        marginTop: 20,
        width: (Dimensions.get('window').width / 4) - 15,
        justifyContent: "center"
    },

    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    balanceContainer: {
        marginTop: 20,
        width: Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15) - 70,
    },

    accountBalance: {
        width: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        height: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        borderRadius: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        backgroundColor: "black",
        borderWidth: 5,
        borderColor: "#36b9cc",
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
        alignItems: "center",
        justifyContent: "center"
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },

    headline: {
        marginTop: 20,
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    promotedProductsContainer: {
        width: (Dimensions.get('window').width / 2.5), 
        marginTop: 0, 
        padding: 0, 
        flexDirection: "row", 
        borderRadius: 20
    },

    promotedProductsImageContainer: {
        width: "40%", 
        backgroundColor: "white", 
        borderTopLeftRadius: 20, 
        borderBottomLeftRadius: 20,
        overflow: "hidden",
    },

    promotedProductsImage: {
        height: 110,
        borderTopLeftRadius: 20, 
        borderBottomLeftRadius: 20,
        backgroundColor: "white",
        resizeMode: "contain",
        overlayColor: "white"
    },

    promotedProductsInfoContainer: {
        width: "60%", 
        backgroundColor: "#36b9cc", 
        borderTopRightRadius: 20, 
        borderBottomRightRadius: 20, 
        padding: 10, 
        justifyContent: "center"
    },

    productContainer: {
        width: (Dimensions.get('window').width / 3) - 22, 
        marginBottom: 10, 
        padding: 5 
    },

    productInfo: {
        position: "absolute", 
        right: 15, 
        zIndex: 2, 
        paddingVertical: 5, 
        paddingHorizontal: 10, 
        backgroundColor: "#36b9cc", 
        borderRadius: 20, 
        alignItems: "center", 
        flexDirection: "row"
    },

    productImage: {
        height: 100,
        resizeMode: "contain",
        width: "100%",
    },

    productName: {
        textAlign: "center", 
        fontWeight: "bold", 
        color: "#676767", 
        marginTop: 5
    },

    favoritesBtn: {
        flexDirection: "row", 
        paddingVertical: 2.5, 
        paddingHorizontal: 10, 
        borderWidth: 1.5, 
        borderColor: "#36b9cc", 
        borderRadius: 15, 
        alignContent: "center", 
        justifyContent: "center",
        width: 100
    },

    alignItemsCenter: {
        alignItems: "center"
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    alignItemsFlexEnd: {
        alignItems: "flex-end"
    },

    justifyContentCenter: {
        justifyContent: "center"
    },

    marginTop5: {
        marginTop: 5
    },

    marginBottom0: {
        marginBottom: 0
    },

    marginBottom20: {
        marginBottom: 20
    },

    marginLeft0: {
        marginLeft: 0
    },

    marginLeft5: {
        marginLeft: 5
    },

    marginLeft10: {
        marginLeft: 10
    },

    marginLeft12: {
        marginLeft: 12
    },

    marginRight5: {
        marginRight: 5
    },

    width50Percent: {
        width: "50%",
    },

    paddingHorizontal10: {
        paddingHorizontal: 10
    },

    limeText: {
        color: "#36b9cc",
        fontSize: 14,
    },

    darkGrayText: {
        color: "#676767",
    },

    textBold: {
        fontWeight: "bold"
    },

    fontSize11: {
        fontSize: 11
    },

    fontSize13: {
        fontSize: 13
    },

    fontSize14: {
        fontSize: 14
    },

    fontSize15: {
        fontSize: 15
    },

    fontSize18: {
        fontSize: 18
    },

    fontSize20: {
        fontSize: 20
    },

    fontSize40: {
        fontSize: 40
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    flxWrap: {
        flexWrap: "wrap"
    },

    justifyContentSpaceBetween: {
        justifyContent: "space-between"
    }

});