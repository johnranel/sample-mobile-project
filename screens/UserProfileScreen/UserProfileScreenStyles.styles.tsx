import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    gradientContainer: {
        flex: 1,
        alignItems: "center"
    },

    mainContainer: {
        flex: 1, 
        width: "100%"
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },

    balanceHeadlineContainer: {
        marginTop: 20,
        width: (Dimensions.get('window').width / 4) - 15,
        justifyContent: "center"
    },

    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    balanceContainer: {
        marginTop: 20,
        width: Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15) - 70,
    },

    accountBalance: {
        width: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        height: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        borderRadius: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        backgroundColor: "black",
        borderWidth: 5,
        borderColor: "#36b9cc",
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
        alignItems: "center",
        justifyContent: "center"
    },

    accountBalanceCustomIcon: {
        alignSelf: "center", 
        borderColor: "#ccc", 
        backgroundColor: "#ccc"
    },

    boxCard: {
        backgroundColor: "black", 
        marginTop: 0,
        width: (Dimensions.get('window').width / 4) - 15,
        height: (Dimensions.get('window').width / 4) - 15,
        borderWidth: 1.5,
        borderColor: "#36b9cc",
        borderRadius: 20,
        padding: 10,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    cardSubHeadline: {
        fontSize: 14,
        fontWeight: "bold",
        color: "#ccc",
        marginBottom: 10,
    },

    navButtons: {
        height: 40, 
        width: "49.5%", 
        backgroundColor: "black", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    limeBoldText: {
        color: "#36b9cc",
        fontSize: 14,
        fontWeight: "bold",
    },

    favoritesBtn: {
        flexDirection: "row", 
        paddingVertical: 2.5, 
        paddingHorizontal: 10, 
        borderWidth: 1.5, 
        borderColor: "#36b9cc", 
        borderRadius: 15, 
        alignContent: "center", 
        justifyContent: "center",
        width: 100
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    flxWrap: {
        flexWrap: "wrap"
    },

    marginTop20: {
        marginTop: 20
    },

    marginBottom5: {
        marginBottom: 5
    },

    marginBottom20: {
        marginBottom: 20
    },

    marginLeft5: {
        marginLeft: 5
    },

    marginLeft12: {
        marginLeft: 12
    },

    alignItemsCenter: {
        alignItems: "center"
    },

    justifyContentCenter: {
        justifyContent: "center"
    },

    fontSize20: {
        fontSize: 20
    },

    fontSize40: {
        fontSize: 40
    },

    width50Percent: {
        width: "50%",
    },

    textAlignRight: {
        textAlign: "right"
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    paddingHorizontal10: {
        paddingHorizontal: 10
    },

    justifyContentSpaceBetween: {
        justifyContent: "space-between"
    }
    
});