import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    gradientContainer: {
        flex: 1,
        alignItems: "center"
    },

    mainContainer: {
        flex: 1, 
        width: "100%"
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },

    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    cardDescription: {
        borderWidth: 1.5, 
        paddingBottom: 3, 
        borderBottomColor: "#fff", 
        borderTopColor: "transparent", 
        borderLeftColor: "transparent", 
        borderRightColor: "transparent"
    },

    balanceHeadlineContainer: {
        marginTop: 20,
        width: (Dimensions.get('window').width / 4) - 15,
        justifyContent: "center"
    },

    balanceContainer: {
        marginTop: 20,
        width: Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15) - 70,
    },

    accountBalance: {
        width: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        height: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        borderRadius: (Dimensions.get('window').width - ((Dimensions.get('window').width / 4) - 15)) / 2,
        backgroundColor: "black",
        borderWidth: 5,
        borderColor: "#36b9cc",
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
        alignItems: "center",
        justifyContent: "center"
    },

    boxCard: {
        backgroundColor: "black", 
        marginTop: 0,
        width: (Dimensions.get('window').width / 4) - 15,
        height: (Dimensions.get('window').width / 4) - 15,
        borderWidth: 1.5,
        borderColor: "#36b9cc",
        borderRadius: 20,
        padding: 10,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    textInput: {
        backgroundColor: "white",
        borderTopRightRadius: 25, 
        borderBottomRightRadius: 25,
        borderTopLeftRadius: 25, 
        borderBottomLeftRadius: 25,
        color: "black",
        borderColor: "#36b9cc",
        borderWidth: 1.5,
        height: 48,
        width: "120%",
    },

    transparentBtn: {
        height: 40,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#13181b",
        borderRadius: 25,
        borderColor: "#36b9cc",
        borderWidth: 1.5,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    whiteText: {
        color: "#36b9cc",
        fontSize: 14,
    },

    textInputCard: {
        backgroundColor: "white",
        borderTopRightRadius: 15, 
        borderBottomRightRadius: 15,
        borderTopLeftRadius: 15, 
        borderBottomLeftRadius: 15,
        padding: 0,
        color: "black",
        height: 30,
        width: "45%",
    },

    cardUpdateBtn: {
        height: 30,
        width: "23%",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#13181b",
        borderRadius: 25,
        borderColor: "#d9ff00",
        borderWidth: 1.5,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    selectionContainer: {
        flexDirection: "row", 
        marginBottom: 5, 
        height: 40, 
        alignItems: "center",
        width: (Dimensions.get('window').width - 25)
    },

    selectionContainerCustom: {
        backgroundColor: "#FBFBFB", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#36b9cc"
    },

    tabSelection: {
        height: 40, 
        width: "22%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25,
    },

    blackBoldText: {
        color: "black",
        fontSize: 14,
        fontWeight: "bold",
    },

    limeBoldText: {
        color: "#36b9cc",
        fontSize: 14,
        fontWeight: "bold",
    },

    lineDivider: {
        borderBottomWidth: 1, 
        borderColor: "black"
    },

    tabTextBold: {
        color: "#36b9cc", 
        fontWeight: "bold"
    },

    tabText: {
        color: "black", 
        fontSize: 14,
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    marginBottom0: {
        marginBottom: 0
    },

    alignItemsCenter: {
        alignItems: "center"
    },

    justifyContentCenter: {
        justifyContent: "center"
    },

    fontSize20: {
        fontSize: 20
    },

    fontSize40: {
        fontSize: 40
    },

    marginTop0: {
        marginTop: 0
    },

    marginLeft5: {
        marginLeft: 5
    },

    marginLeft12: {
        marginLeft: 12
    },

    marginLeft15: {
        marginLeft: 15
    },

    padding5: {
        padding: 5
    },

    paddingX5: {
        paddingLeft: 5,
        paddingRight: 5
    },

    width20Percent: {
        width: "20%",
    },

    width40Percent: {
        width: "40%",
    },

    width48Percent: {
        width: "48%",
    },

    textAlignCenter: {
        textAlign: "center"
    },

    paddingHorizontal10: {
        paddingHorizontal: 10
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    grayText: {
        color: "#676767"
    },

    bgBlack: {
        backgroundColor: "black",
    },

    bgTransparent: {
        backgroundColor: "transparent",
    },

    justifyContentSpaceBetween: {
        justifyContent: "space-between"
    }
});