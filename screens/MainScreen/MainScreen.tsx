import React from "react";

import {
    RefreshControl,
    SafeAreaView,
    ScrollView,
    View,
    Image,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";

import { Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { useFocusEffect } from '@react-navigation/native';

import TopNavigator from "screens/TopNavigator/TopNavigator";

import styles from "./MainScreenStyles.styles";

import axios from "services/api";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { AppContext } from "../../context/AppContext";

const MainScreen = ({ navigation }) => {
    const [userPoints, setUserPoints] = React.useState(0);

    const [listItems, setListItems] = React.useState([]);
    const [listPromotedItems, setListPromotedItems] = React.useState([]);

    const [offsetProductsData, setOffsetProductsData] = React.useState(0);
    const [offsetProductsPromotedData, setOffsetProductsPromotedData] = React.useState(0);
    
    const [lockVerticalLoading, setLockVerticalLoading] = React.useState(false);
    const [noMoreVerticalData, setNoMoreVerticalData] = React.useState(false);

    const [lockHorizontalLoading, setLockHorizontalLoading] = React.useState(false);
    const [noMoreHorizontalData, setNoMoreHorizontalData] = React.useState(false);

    const [refreshing, setRefreshing] = React.useState(false);

    const onRefresh = React.useCallback(async () => {
        setRefreshing(true);

        setListItems([]);
        setListPromotedItems([]);
        setOffsetProductsData(0);
        setOffsetProductsPromotedData(0);
        setNoMoreVerticalData(false);

        await asyncProductsFetch();
        await asyncPromotedProductsFetch();

        setTimeout(() => {
            setRefreshing(false);
        }, 500);
    }, []);

    const contextData = React.useContext(AppContext);

    useFocusEffect(
        React.useCallback(() => {
            asyncDataFetch();
        }, [])
    );

    React.useEffect(() => {
        asyncProductsFetch();
        asyncPromotedProductsFetch();
    }, []);

    let asyncDataFetch = async () => {
        let userParsedData = await AsyncStorage.getItem("user");
        let user_data = JSON.parse(userParsedData);

        let userPointsData = await AsyncStorage.getItem("user_points");
        let userPointsParsed = JSON.parse(userPointsData);
        setUserPoints(userPointsParsed);
    };

    let asyncProductsFetch = async () => {
        setLockVerticalLoading(true);
        await axios(contextData.userAccessTokenContext)
            .get(`/products/get/${offsetProductsData}`)
            .then(async(res) => {
                (listItems.length != 0) ? setListItems(listItems.concat(res.data.product_list)) : setListItems(res.data.product_list);
                setOffsetProductsData(offsetProductsData+6);
                setLockVerticalLoading(false);
                if (res.data.product_list.length !== 6) {
                    setNoMoreVerticalData(true);
                }
            }).catch((err) => {
                setLockVerticalLoading(false);
                setNoMoreVerticalData(true);
            });
    };

    let asyncPromotedProductsFetch = async () => {
        setLockHorizontalLoading(true);
        await axios(contextData.userAccessTokenContext)
            .get(`/products/promoted/get/${offsetProductsPromotedData}`)
            .then(async(res) => {
                (listPromotedItems.length != 0) ? setListPromotedItems(listPromotedItems.concat(res.data.discounted_product_list)) : setListPromotedItems(res.data.discounted_product_list);
                setOffsetProductsPromotedData(offsetProductsPromotedData+3);
                setLockHorizontalLoading(false);
                if (res.data.discounted_product_list.length !== 3) {
                    setNoMoreHorizontalData(true);
                }
            }).catch((err) => {
                setLockHorizontalLoading(false);
                setNoMoreHorizontalData(true);
            });
    };

    let isCloseToEnd = ({layoutMeasurement, contentOffset, contentSize}) => {
        if(!lockHorizontalLoading) {
            let paddingToRight = 10;
            return layoutMeasurement.width + contentOffset.x >= contentSize.width - paddingToRight;
        } else {
            return false;
        }
    };

    let isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        if(!lockVerticalLoading) {
            let paddingToBottom = 10;
            return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
        } else {
            return false;
        }
    };

    const productDetailScreen = (item) => {
        navigation.navigate("ProductDetailScreen", item);
    };

    const productScreen = () => {
        navigation.navigate("ProductScreen");
    };

    const prioritiesScreen = () => {
        navigation.navigate("PrioritiesScreen");
    };

    const billingInformationScreen = () => {
        navigation.navigate("BillingInformationScreen");
    };

    const gymFranchiseScreen = () => {
        navigation.navigate("GymFranchiseScreen");
    }

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={styles.gradientContainer}>
            <SafeAreaView style={styles.mainContainer}>
                <ScrollView style={styles.mainContainer} 
                    onScroll={({nativeEvent}) => {
                        if (isCloseToBottom(nativeEvent)) {
                            asyncProductsFetch();
                        }
                    }}
                    refreshControl={
                        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                    }>
                    <View style={styles.alignItemsCenter}>
                        <TopNavigator refresh={true} subpage="false" />
                        <View style={styles.card}>
                            <Image
                                style={styles.logo}
                                source={require("../../assets/logo.png")}
                            />
                        </View>
                        <View style={{ flexDirection: "row" }}>
                            <TouchableOpacity style={[styles.boxCard, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={productScreen}>
                                <Text style={[styles.limeText, styles.fontSize13]}>Products</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.boxCard, styles.marginLeft12, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={prioritiesScreen}>
                                <Text style={[styles.limeText, styles.fontSize13]}>Goals &{'\n'}Training</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.boxCard, styles.marginLeft12, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={billingInformationScreen}>
                                <Text style={[styles.limeText, styles.fontSize13]}>Account &{'\n'}Top-ups</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.boxCard, styles.marginLeft12, styles.alignItemsCenter, styles.justifyContentCenter]} onPress={gymFranchiseScreen}>
                                <Text style={[styles.limeText, styles.fontSize13]}>Gyms &{'\n'}Franchises</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: "row" }}>
                            <View style={styles.balanceHeadlineContainer}>
                                <Text style={styles.cardHeadline}>Account Balance</Text>
                            </View>
                            <View style={styles.balanceContainer}>
                                <View style={styles.accountBalance}>
                                    <Text style={[styles.limeText, styles.textBold, styles.fontSize20]}>Points</Text>
                                    <Text style={[styles.limeText, styles.textBold, styles.fontSize40]}>{(userPoints != null) ? userPoints.points : 0}</Text>
                                    <Text style={[styles.limeText, styles.textBold, styles.fontSize20]}>{(userPoints != null) ? userPoints.amount : 0} AUD</Text>
                                </View>
                            </View>
                        </View>
                        {listPromotedItems.length > 0 && (
                            <View style={styles.cardNoBg}>
                                <Text style={[styles.cardHeadline, styles.marginBottom0]}>Promotions</Text>
                                <ScrollView style={styles.flxDrtnRow} contentContainerStyle={{ paddingTop: 10, paddingBottom: 20 }} 
                                    horizontal 
                                    showsHorizontalScrollIndicator={false} 
                                    onScroll={({nativeEvent}) => {
                                        if (isCloseToEnd(nativeEvent)) {
                                            asyncPromotedProductsFetch();
                                        }
                                    }}
                                    scrollEventThrottle={400}>
                                    {
                                        listPromotedItems.map((item, index) => {
                                            return (
                                                <TouchableOpacity key={item.id} style={[styles.card, styles.promotedProductsContainer, (index !=0) ? styles.marginLeft5 : styles.marginLeft0 ]} onPress={() => productDetailScreen(item)}>
                                                    <View style={styles.promotedProductsImageContainer}>
                                                        <Image source={(item.media.length > 0) ? { uri: item.media[0].path } : require("../../assets/placeholder-supplement.jpg")} style={styles.promotedProductsImage} />
                                                    </View>
                                                    <View style={styles.promotedProductsInfoContainer}>
                                                        <View style={[styles.flxDrtnRow, styles.alignItemsCenter, styles.justifyContentCenter]}>
                                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.fontSize18, styles.textBold]}>
                                                                {item.price_in_points}
                                                            </Text>
                                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.fontSize14, styles.textBold, styles.marginLeft5, styles.darkGrayText]}>
                                                                {item.price_in_aud} AUD
                                                            </Text>
                                                        </View>
                                                        <Text adjustsFontSizeToFit={true} numberOfLines={2} style={[styles.marginTop5, styles.textBold]}>
                                                            {item.name}
                                                        </Text>
                                                    </View>
                                                </TouchableOpacity>
                                            );
                                        })
                                    }
                                    {listPromotedItems.length <= 0 && lockHorizontalLoading == false && (
                                        <Text style={[styles.alignSelfCenter, styles.paddingHorizontal10]}>No available product.</Text>
                                    )}
                                    {!noMoreHorizontalData && (
                                        <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                                    )}
                                </ScrollView>
                            </View>
                        )}
                        <View style={[styles.card, styles.marginBottom20]}>
                            <View style={[styles.flxDrtnRow, styles.justifyContentSpaceBetween]}>
                                <Text style={styles.cardHeadline}>Products</Text>
                                <View style={styles.cardHeadline}>
                                    {/* <TouchableOpacity style={styles.favoritesBtn}>
                                        <MaterialCommunityIcons
                                            style={{ marginRight: 2 }}
                                            name={"heart"}
                                            color={"#C9EA3B"}
                                            size={14}
                                        /> 
                                        <Text>Favorites</Text>
                                    </TouchableOpacity> */}
                                </View>
                            </View>
                            {listItems.length > 0 && (
                                <View style={[styles.flxDrtnRow, styles.flxWrap]}>
                                {
                                    listItems.map((item, index) => {
                                        return (
                                            <View key={item.id} style={[styles.productContainer]}>
                                                <TouchableOpacity onPress={() => productDetailScreen(item)}>
                                                    <View style={styles.productInfo}>
                                                        <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.textBold, styles.marginRight5]}>{ item.price_in_points }</Text>
                                                        <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.textBold, styles.fontSize11]}>{item.price_in_aud} AUD</Text>
                                                    </View>
                                                    <Image source={(item.media.length > 0) ? { uri: item.media[0].path } : require("../../assets/placeholder-supplement.jpg")} style={styles.productImage}/>
                                                    <Text style={styles.productName}>
                                                        {item.name}
                                                    </Text>
                                                </TouchableOpacity>
                                            </View>
                                        );
                                    })
                                }
                                </View>
                            )}
                            {listItems.length <= 0 && lockVerticalLoading == false && (
                                <Text style={styles.alignSelfCenter}>No available product.</Text>
                            )}
                            {!noMoreVerticalData && (
                                <ActivityIndicator size="large" color="#C9EA3B" style={[styles.alignSelfCenter, styles.paddingHorizontal10]} />
                            )}
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default MainScreen;