import React from "react";
import {
    Text,
    View,
    Pressable,
    TouchableOpacity,
    Modal,
    ActivityIndicator,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    Platform
} from "react-native";

import { TextInput } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";

import styles from "./ResetPasswordScreenStyles.styles";
import { useTogglePasswordVisibility } from "hooks/useTogglePasswordVisibility";
import axios from "services/api";

const ResetPasswordScreen = ({ route, navigation }) => {
    const { passwordVisibility, passwordRightIcon, handlePasswordVisibility } = useTogglePasswordVisibility();
    const { confirmPasswordVisibility, confirmPasswordRightIcon, handleConfrimPasswordVisibility } = useTogglePasswordVisibility();
    const [password, setPassword] = React.useState("");
    const [confirm_password, setConfirmPassword] = React.useState("");
    const [error_password, setErrorPassword] = React.useState("");
    const [error_confirm_password, setErrorConfirmPassword] = React.useState("");
    const [loading, setLoading] = React.useState(false);
    const [showModal, setShowModal] = React.useState(false);
    const [requestStatus, setRequestStatus] = React.useState("");
    const [requestMessage, setRequestMessage] = React.useState("");


    let validateData = () => {
        if (password != confirm_password) {
            setErrorConfirmPassword(
                "Password and Confirm password must be the same"
            );
        } else if (password.length < 8) {
            setErrorPassword("Password must contain 8 or more characters");
        } else if (password === confirm_password) {
            setErrorPassword("");
            setErrorConfirmPassword("");
            resetPassword();
        }
    };

    let resetPassword = async () => {
        const data = { email: route.params.email, password: password, password_confirmation: confirm_password, token: route.params.token };
        setShowModal(true);
        setLoading(true);
        let response = await axios()
            .post("/reset-password", data)
            .then(async (res) => {
                setRequestStatus("Password changed.");
                setRequestMessage("Successfully changed your password.");
                setLoading(false);
            })
            .catch((err) => {
                setRequestStatus("Error occurred.");
                setRequestMessage("Encountered an error while changing your password.");
                setLoading(false);
            });
    };

    let landingScreen = () => {
        navigation.replace("LoginScreen");
    };

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={{ flex: 1 }}>
            <KeyboardAvoidingView style={styles.mainContainer} behavior={Platform.OS === "ios" ? "padding" : "height"} enabled>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.card}>
                        <View style={styles.container}>
                            <Text style={styles.cardHeadline}>Enter your new password.</Text>
                            <Text style={styles.cardDescription}>
                                Choose a new password with at least 8 characters.
                            </Text>
                        </View>
                        <View style={styles.fieldsContainer}>
                            <TextInput
                                style={styles.textInput}
                                label="Password"
                                value={password}
                                secureTextEntry={passwordVisibility}
                                underlineColor="transparent"
                                selectionColor="black"
                                activeUnderlineColor="transparent"
                                onChangeText={(text) => setPassword(text)}
                            />
                            <View style={styles.passwordViewContainer}>
                                <Pressable onPress={handlePasswordVisibility}>
                                    <MaterialCommunityIcons
                                        name={passwordRightIcon}
                                        size={22}
                                        color="#232323"
                                    />
                                </Pressable>
                            </View>
                        </View>
                        {error_password != "" && (
                            <Text style={styles.errorMessage}>{error_password}</Text>
                        )}
                        <View style={styles.fieldsContainer}>
                            <TextInput
                                style={styles.textInput}
                                label="Confirm Password"
                                value={confirm_password}
                                secureTextEntry={confirmPasswordVisibility}
                                underlineColor="transparent"
                                selectionColor="black"
                                activeUnderlineColor="transparent"
                                onChangeText={(text) => setConfirmPassword(text)}
                            />
                            <View style={styles.passwordViewContainer}>
                                <Pressable onPress={handleConfrimPasswordVisibility}>
                                    <MaterialCommunityIcons
                                        name={confirmPasswordRightIcon}
                                        size={22}
                                        color="#232323"
                                    />
                                </Pressable>
                            </View>
                        </View>
                        {error_confirm_password != "" && (
                            <Text style={styles.errorMessage}>{error_confirm_password}</Text>
                        )}

                        <View style={[styles.container, styles.marginTop15]}>
                            <TouchableOpacity
                                style={styles.transparentBtn}
                                onPress={() => validateData()}
                            >
                                <Text style={styles.whiteText}>RESET</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={styles.marginTop10}
                                onPress={landingScreen}
                            >
                                <Text style={styles.textDecorationUnderline}>Go back</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={showModal}
                    onRequestClose={() => {
                        setShowModal(false);
                    }}
                >
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            {(loading &&
                                <View style={styles.modalLoadingContainer}>
                                    <ActivityIndicator size="large" color="#12bcc9" />
                                    <Text>Loading...</Text>
                                </View>
                            )}
                            {(!loading &&
                                <View>
                                    <Text style={styles.modalTitle}>{requestStatus}</Text>
                                    <Text style={styles.modalMessage}>{requestMessage}</Text>
                                    <TouchableOpacity
                                        style={[styles.button, styles.buttonClose]}
                                        onPress={() => { landingScreen() }}
                                    >
                                        <Text style={styles.buttonCloseTextStyle}>Close</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                        </View>
                    </View>
                </Modal>
            </KeyboardAvoidingView>
        </LinearGradient>
    );
};

export default ResetPasswordScreen;
