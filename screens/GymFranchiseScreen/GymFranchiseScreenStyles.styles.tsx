import { StyleSheet, Dimensions, Platform } from "react-native";

export default StyleSheet.create({
    gradientContainer: {
        flex: 1,
        alignItems: "center"
    },

    mainContainer: {
        flex: 1, 
        width: "100%"
    },

    logo: {
        width: "100%", 
        resizeMode: "contain", 
        height: 120
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },
    
    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    cardDescription: {
        color: "#676767", 
        marginBottom: 10
    },

    selectionButton: {
        height: 20, 
        width: "20%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#ccc",
    },

    selectionContainer: {
        width: "60%", 
        alignItems: "flex-end"
    },

    selectionBlackButton: {
        height: 20, 
        alignItems: "center", 
        justifyContent: "flex-end", 
        borderRadius: 25, 
        flexDirection: "row", 
        backgroundColor: "black", 
        paddingHorizontal: 7,
        marginRight: 20
    },

    mapContainer: {
        borderRadius: 20, 
        marginBottom: 20,  
        shadowOffset: { width: 0, height: 0 }, 
        shadowColor: "#000", 
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1, 
        shadowRadius: 5, 
        elevation: 10, 
        overflow: "hidden"
    },

    map: {
        height: 200, 
        width: "100%"
    },

    gymFranchise: {
        flexDirection: "row", 
        marginBottom: 5, 
        alignItems: "center", 
        flexWrap: "wrap",
    },

    selectionPillsBtn: {
        height: 40, 
        width: "31.6%", 
        marginLeft: 5, 
        marginBottom: 5, 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    blackText: {
        color: "black",
        fontSize: 14
    },

    blackBoldText: {
        color: "black",
        fontSize: 14,
        fontWeight: "bold",
    },

    limeText: {
        color: "#36b9cc",
        fontSize: 14,
    },

    limeBoldCenterText: {
        color: "#36b9cc",
        fontSize: 14,
        fontWeight: "bold",
        textAlign: "center"
    },

    blackCenterText: {
        color: "black",
        fontSize: 14,
        textAlign: "center"
    },

    bgBlackBorderLimeCustom: {
        borderWidth: 1.5, 
        borderColor: "#36b9cc",
        backgroundColor: "#000"
    },

    borderCustom: {
        borderWidth: 1.5, 
        borderColor: "#ccc"
    },

    selectionTabsBtn: {
        flexDirection: "row", 
        padding: 10, 
        width: "100%", 
        marginBottom: 5, 
        borderRadius: 25, 
        borderWidth: 1.5
    },

    modalContainer: {
        flex: 1, 
        alignItems: "center", 
        justifyContent: "center"
    },

    modalBtn: {
        height: 40, 
        width: "49%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    modalLimeBtn: {
        borderWidth: 1.5, 
        borderColor: "#36b9cc",
        backgroundColor: "black",
        marginLeft: 5
    },

    modalBtnCstm: {
        borderWidth: 1.5, 
        borderColor: "#ccc", 
        marginLeft: 5
    },

    searchDropdown: {
        padding: 10,
        marginTop: 2,
        backgroundColor: '#ddd',
        borderColor: '#bbb',
        borderWidth: 1,
        borderRadius: 5,
    },

    bgBlack: {
        backgroundColor: "black",
    },

    alignItemsCenter: {
        alignItems: "center"
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    paddingRight2: {
        paddingRight: 2
    },

    marginTop0: {
        marginTop: 0
    },

    marginTop10: {
        marginTop: 10
    },

    marginRight20: {
        marginRight: 20
    },

    marginBottom5: {
        marginBottom: 5
    },

    marginBottom10: {
        marginBottom: 10
    },

    marginBottom20: {
        marginBottom: 20
    },

    marginLeft2: {
        marginLeft: 2
    },

    marginLeft5: {
        marginLeft: 5
    },

    marginVertical0: {
        marginVertical: 0
    },

    marginVertical10: {
        marginVertical: 10
    },

    width50Percent: {
        width: "50%",
    },

    width80Percent: {
        width: "80%",
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    paddingHorizontal10: {
        paddingHorizontal: 10
    },

    textBold: {
        fontWeight: "bold"
    },

    fontSize11: {
        fontSize: 11
    },

    paddingX2: {
        paddingLeft: 2,
        paddingRight: 2
    },

    errorMessage: {
        color: "red",
        fontSize: 14,
    },
});