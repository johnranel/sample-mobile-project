import axios from 'axios';

const api = (token = null) => axios.create({
    baseURL: "http://192.168.0.9:80/api",
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json',
        'Authorization': "Bearer " + token
    },
    withCredentials: true,
});

export default api;