import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    gradientContainer: {
        flex: 1,
        alignItems: "center"
    },

    mainContainer: {
        flex: 1, 
        width: "100%"
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 25,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    cardHeadline: {
        fontSize: 30,
        fontWeight: "bold",
        marginBottom: 10,
    },

    cardDescription: {
        fontSize: 14,
        color: "#999",
        lexWrap: 'wrap', 
        lineHeight: 18, 
        textAlign: 'left'
    },

    alignItemsCenter: { 
        alignItems: "center" 
    },

    marginBottom20: {
        marginBottom: 20
    },
});