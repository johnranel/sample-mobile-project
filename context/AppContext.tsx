import React from "react";

const AppContext = React.createContext();
import AsyncStorage from "@react-native-async-storage/async-storage";

const AppProvider = ({children}) => {
    const [userDataContext, setUserDataContext] = React.useState();
    const [userAccessTokenContext, setUserAccessTokenContext] = React.useState(null);

    React.useEffect(() => {
        setContextData();
    },[]);

    let setContextData = async () => {
        const userDataString = await AsyncStorage.getItem("user");
        const userAccessToken = await AsyncStorage.getItem("accessToken");
        const parsedUserData = JSON.parse(userDataString);

        setUserDataContext(parsedUserData);
        setUserAccessTokenContext(userAccessToken);
    };

    return (
        <AppContext.Provider value={{userDataContext, setUserDataContext, userAccessTokenContext, setUserAccessTokenContext}}>
            {children}
        </AppContext.Provider>
    );
}

export {AppContext, AppProvider}