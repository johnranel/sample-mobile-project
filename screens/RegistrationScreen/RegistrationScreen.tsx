import React from "react";
import {
    SafeAreaView,
    ScrollView,
    View,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Keyboard,
    TouchableOpacity,
    Image,
    Platform,
    Pressable
} from "react-native";

import { TextInput, Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";

import DateTimePicker from '@react-native-community/datetimepicker';
import { CountryPicker } from "react-native-country-codes-picker";
import * as ImagePicker from 'expo-image-picker';

import styles from "./RegistrationScreenStyles.styles";
import { useTogglePasswordVisibility } from "hooks/useTogglePasswordVisibility";

import moment from 'moment';

const RegistrationScreen = ({ navigation }) => {
    const [firstname, setFirstname] = React.useState("");
    const [lastname, setLastname] = React.useState("");
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [gender, setGender] = React.useState("male");
    const [address, setAddress] = React.useState("");
    const [mobileNumber, setMobileNumber] = React.useState("");
    const [date, setDate] = React.useState(new Date());
    const [datePickerShow, setDatePickerShow] = React.useState(false);
    const [currentWeight, setCurrentWeight] = React.useState(0);
    const [levelOfFitness, setLevelOfFitness] = React.useState("beginner");
    const [gymAttendance, setGymAttendance] = React.useState("casual");
    const [image, setImage] = React.useState(null);

    const [errorFirstname, setErrorFirstname] = React.useState("");
    const [errorLastname, setErrorLastname] = React.useState("");
    const [errorEmail, setErrorEmail] = React.useState("");
    const [errorPassword, setErrorPassword] = React.useState("");
    const [errorAddress, setErrorAddress] = React.useState("");
    const [errorMobileNumber, setErrorMobileNumber] = React.useState("");
    const [errorDate, setErrorDate] = React.useState("");
    const [errorCurrentWeight, setErrorCurrentWeight] = React.useState("");

    const { passwordVisibility, passwordRightIcon, handlePasswordVisibility } = useTogglePasswordVisibility();

    const [showCountryCodeSelect, setShowCountryCodeSelect] = React.useState(false);
    const [countryCode, setCountryCode] = React.useState('+61');

    const registrationData = {
        first_name: firstname,
        last_name: lastname,
        email: email,
        image: image,
        password: password,
        gender: gender,
        address: address,
        mobile_number: countryCode + mobileNumber,
        birthdate: moment(date).format("LL"),
        current_weight: currentWeight,
        level_of_fitness: levelOfFitness,
        gym_attendance: gymAttendance
    };

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            base64: true,
            aspect: [1, 1],
            quality: 1,
        });

        if (!result.canceled) {
            setImage("data:image/jpeg;base64,"+result.assets[0].base64);
        }
    };

    const validateData = () => {
        (firstname.length < 2) ? setErrorFirstname("First name must be two or more characters.") : setErrorFirstname("");
        
        (lastname.length < 2) ? setErrorLastname("Last name must be two or more characters.") : setErrorLastname("");

        let emailRegex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
        (!emailRegex.test(String(email).toLowerCase())) ? setErrorEmail("Please use a valid email.") : setErrorEmail("");

        let passwordRegex = new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W\_])[A-Za-z\d\W\_]{8,}$/);
        (!passwordRegex.test(password)) ? setErrorPassword("Password must have - 8 characters, 1 uppercase letter, 1 number and 1 special character.") : setErrorPassword("");

        (address.length < 2) ? setErrorAddress("Address must be two or more characters.") : setErrorAddress("");
        (mobileNumber.length < 2) ? setErrorMobileNumber("Mobile number must be two or more characters.") : setErrorMobileNumber("");
        (currentWeight <= 0) ? setErrorCurrentWeight("Weight must be set.") : setErrorCurrentWeight("");

        moment(date).isAfter(new Date()) ? setErrorDate("Date must not exceed the current date.") : (moment(date).format("LL") === moment(new Date()).format("LL")) ? setErrorDate("Date must not be equal to current date.") : setErrorDate("");

        (firstname.length >= 2 && 
        lastname.length >= 2 && 
        emailRegex.test(String(email).toLowerCase()) && 
        passwordRegex.test(password) && 
        address.length >= 2 && 
        mobileNumber.length >= 2 && 
        currentWeight > 0  && 
        moment(date).isBefore(new Date()) && 
        moment(date).format("LL") !== moment(new Date()).format("LL")) ? gymSelectionScreen() : '';
    };

    const gymSelectionScreen = () => {
        navigation.navigate("GymSelectionScreen", registrationData);
    };

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={{ flex: 1 }}>
            <SafeAreaView>
                <ScrollView>
                    <KeyboardAvoidingView style={styles.mainContainer} behavior={Platform.OS === "ios" ? "padding" : undefined} keyboardVerticalOffset={55} enabled>
                        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                            <>
                                <View style={styles.card}>
                                    <Text style={styles.cardHeadline}>Sign up</Text>
                                    <View style={styles.zIndex2}>
                                        <TextInput
                                            style={styles.textInput}
                                            placeholder="Address (Suburb) *"
                                            underlineColor="transparent"
                                            selectionColor="black"
                                            activeUnderlineColor="transparent"
                                            onChangeText={(text) => setAddress(text)}
                                        />
                                        {errorAddress != "" && (
                                            <Text style={styles.errorMessage}>{errorAddress}</Text>
                                        )}
                                    </View>
                                    <View style={[styles.zIndex2, styles.flxDrtnRow, styles.justifyContentSpaceBetween]}>
                                        <TouchableOpacity style={styles.countryCode} onPress={() => setShowCountryCodeSelect(true)}>
                                            <Text>
                                                { countryCode }
                                            </Text>
                                        </TouchableOpacity>
                                        <TextInput
                                            style={[styles.textInput, styles.width85percent]}
                                            placeholder="Phone/Mobile no. *"
                                            underlineColor="transparent"
                                            selectionColor="black"
                                            activeUnderlineColor="transparent"
                                            keyboardType="numeric"
                                            onChangeText={(text) => setMobileNumber(text)}
                                        />
                                        {errorMobileNumber != "" && (
                                            <Text style={styles.errorMessage}>{errorMobileNumber}</Text>
                                        )}
                                        <CountryPicker
                                            show={showCountryCodeSelect}
                                            // when picker button press you will get the country object with dial code
                                            pickerButtonOnPress={(item) => {
                                                setCountryCode(item.dial_code);
                                                setShowCountryCodeSelect(false);
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={styles.card}>
                                    <Text style={styles.cardHeadline}>Personal Information</Text>
                                    <View style={styles.personalInfoContainer}>
                                        <View style={[styles.grayImageContainer, styles.marginBottom5]}>
                                            <TouchableOpacity style={styles.grayImagePickerBtn} onPress={pickImage}>
                                                <Image source={(image) ? { uri: image } : require("../../assets/black-pic.png")} style={styles.grayImagePickerImg} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={styles.personalInfoFieldsContainer}>
                                            <TextInput
                                                style={styles.textInput}
                                                placeholder="First name *"
                                                underlineColor="transparent"
                                                selectionColor="black"
                                                activeUnderlineColor="transparent"
                                                onChangeText={(text) => setFirstname(text)}
                                            />
                                            {errorFirstname != "" && (
                                                <Text style={styles.errorMessage}>{errorFirstname}</Text>
                                            )}
                                            <TextInput
                                                style={styles.textInput}
                                                placeholder="Last name *"
                                                underlineColor="transparent"
                                                selectionColor="black"
                                                activeUnderlineColor="transparent"
                                                onChangeText={(text) => setLastname(text)}
                                            />
                                            {errorLastname != "" && (
                                                <Text style={styles.errorMessage}>{errorLastname}</Text>
                                            )}
                                            <TextInput
                                                style={styles.textInput}
                                                placeholder="Email *"
                                                autoCapitalize="none"
                                                underlineColor="transparent"
                                                selectionColor="black"
                                                activeUnderlineColor="transparent"
                                                onChangeText={(text) => setEmail(text)}
                                            />
                                            {errorEmail != "" && (
                                                <Text style={styles.errorMessage}>{errorEmail}</Text>
                                            )}
                                        </View>
                                    </View>
                                    <View style={styles.zIndex2}>
                                        <View style={[styles.fieldsContainer, styles.passwordInputContainer]}>
                                            <TextInput
                                                style={[styles.textInput, styles.borderColorTransparent, styles.marginBottom0, styles.width90percent]}
                                                placeholder="Password *"
                                                autoCapitalize="none"
                                                underlineColor="transparent"
                                                selectionColor="black"
                                                activeUnderlineColor="transparent"
                                                secureTextEntry={passwordVisibility}
                                                onChangeText={(text) => setPassword(text)}
                                            />
                                            <View style={styles.passwordViewContainer}>
                                                <Pressable onPress={handlePasswordVisibility}>
                                                    <MaterialCommunityIcons
                                                        name={passwordRightIcon}
                                                        size={22}
                                                        color="#232323"
                                                    />
                                                </Pressable>
                                            </View>
                                        </View>
                                        <Text style={styles.marginBottom10}>Password must have - 8 characters, 1 uppercase letter, 1 number and 1 special character.</Text>
                                        <View style={styles.selectionContainer}>
                                            <Text style={styles.fieldLabel}>
                                                Date of Birth *
                                            </Text>
                                            <TouchableOpacity style={styles.dateCustomBtn} onPress={() => { setDatePickerShow(true); }}>
                                                <Text>{moment(date).format("LL")}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        {errorDate != "" && (
                                            <Text style={styles.errorMessage}>{errorDate}</Text>
                                        )}
                                        {datePickerShow && Platform.OS == "ios" && (
                                            <>
                                                <DateTimePicker
                                                    testID="dateTimePicker"
                                                    value={date}
                                                    mode={'date'}
                                                    display="inline"
                                                    onChange={(event, selectedDate) => {
                                                        setDate(selectedDate);
                                                    }}
                                                />
                                                <TouchableOpacity style={[styles.blackLimeOutlineBtn, { width: "auto", marginTop: 0 }]} onPress={() => { setDatePickerShow(false); }}>
                                                    <Text style={styles.limeText}>Confirm Date</Text>
                                                </TouchableOpacity>
                                            </>
                                        )}
                                        <View style={[styles.selectionContainer, styles.selectionContainerCustom]}>
                                            <TouchableOpacity style={[styles.tabSelection, (gender == "male") ? styles.bgBlack : styles.bgTransparent]} onPress={() => { setGender("male") }}>
                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (gender == "male") ? styles.tabTextBold : styles.tabText]}>Male</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (gender == "female") ? styles.bgBlack : styles.bgTransparent]} onPress={() => { setGender("female") }}>
                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (gender == "female") ? styles.tabTextBold : styles.tabText]}>Female</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (gender == "not defined") ? styles.bgBlack : styles.bgTransparent]} onPress={() => { setGender("not defined") }}>
                                                <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (gender == "not defined") ? styles.tabTextBold : styles.tabText]}>Better not say</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.card}>
                                    <Text style={styles.cardHeadline}>Athlete Profile</Text>
                                    <View style={styles.selectionContainer}>
                                        <Text style={styles.fieldLabel}>
                                            Current Weight in kg *
                                        </Text>
                                        <TextInput
                                            style={[styles.textInput, styles.currentWeightCustomFld]}
                                            placeholder="0"
                                            underlineColor="transparent"
                                            selectionColor="black"
                                            activeUnderlineColor="transparent"
                                            keyboardType="numeric"
                                            onChangeText={(text) => setCurrentWeight(text)}
                                        />
                                    </View>
                                    {errorCurrentWeight != "" && (
                                        <Text style={styles.errorMessage}>{errorCurrentWeight}</Text>
                                    )}
                                    <Text style={styles.cardHeadline}>Level of Fitness</Text>
                                    <View style={[styles.selectionContainer, styles.selectionContainerCustom]}>
                                        <TouchableOpacity style={[styles.tabSelection, (levelOfFitness == "beginner") ? styles.bgBlack : styles.bgTransparent]} onPress={() => setLevelOfFitness("beginner") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (levelOfFitness == "beginner") ? styles.tabTextBold : styles.tabText]}>Beginner</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (levelOfFitness == "intermediate") ? styles.bgBlack : styles.bgTransparent]} onPress={() => setLevelOfFitness("intermediate") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (levelOfFitness == "intermediate") ? styles.tabTextBold : styles.tabText]}>Intermediate</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.tabSelection, styles.marginLeft15, (levelOfFitness == "advanced") ? styles.bgBlack : styles.bgTransparent]} onPress={() => setLevelOfFitness("advanced") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.paddingX5, (levelOfFitness == "advanced") ? styles.tabTextBold : styles.tabText]}>Advanced</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Text style={styles.cardHeadline}>Gym Attendance</Text>
                                    <View style={{ flexDirection: "column", marginBottom: 5, alignItems: "center", }}>
                                        <TouchableOpacity style={[styles.tabSelectionFullWidth, styles.marginBottom5, (gymAttendance == "casual") ? styles.bgBlack : styles.bgLime]} onPress={() => setGymAttendance("casual") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.tabTextFullWidth, (gymAttendance == "casual") ? styles.limeText : styles.tabText]}>Casual</Text>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[(gymAttendance == "casual") ? styles.limeTextPad : styles.tabTextPad]}>1-2 days per week</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.tabSelectionFullWidth, styles.marginBottom5,, (gymAttendance == "regular") ? styles.bgBlack : styles.bgLime]} onPress={() => setGymAttendance("regular") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.tabTextFullWidth, (gymAttendance == "regular") ? styles.limeText : styles.tabText]}>Regular</Text>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[(gymAttendance == "regular") ? styles.limeTextPad : styles.tabTextPad]}>3-4 days per week</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={[styles.tabSelectionFullWidth, (gymAttendance == "enthusiast") ? styles.bgBlack : styles.bgLime]} onPress={() => setGymAttendance("enthusiast") }>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[styles.tabTextFullWidth, (gymAttendance == "enthusiast") ? styles.limeText : styles.tabText]}>Enthusiast</Text>
                                            <Text adjustsFontSizeToFit={true} numberOfLines={1} style={[(gymAttendance == "enthusiast") ? styles.limeTextPad : styles.tabTextPad]}>5-7 days per week</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <TouchableOpacity style={styles.blackLimeOutlineBtn} onPress={validateData}>
                                    <Text style={styles.limeText}>NEXT</Text>
                                </TouchableOpacity>
                                {datePickerShow && Platform.OS == "android" && (
                                    <DateTimePicker
                                        testID="dateTimePicker"
                                        value={date}
                                        mode={'date'}
                                        onChange={(event, selectedDate) => {
                                            setDatePickerShow(false);
                                            setDate(selectedDate);
                                        }}
                                    />
                                )}
                            </>
                        </TouchableWithoutFeedback>
                    </KeyboardAvoidingView>
                </ScrollView>
            </SafeAreaView>
        </LinearGradient>
    );
};

export default RegistrationScreen;