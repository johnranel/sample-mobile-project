import React from "react";
import {
    View,
    KeyboardAvoidingView,
    TouchableWithoutFeedback,
    Image,
    Keyboard,
    TouchableOpacity,
    ActivityIndicator,
    Platform,
    Pressable
} from "react-native";

import { TextInput, Text } from "react-native-paper";
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons } from "@expo/vector-icons";

import Checkbox from 'expo-checkbox';
import styles from "./LoginScreenStyles.styles";
import axios from "services/api";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { useTogglePasswordVisibility } from "hooks/useTogglePasswordVisibility";
import { AppContext } from "../../context/AppContext";

const LoginScreen = ({ navigation }) => {
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [loading, setLoading] = React.useState(false);
    const [error_msg, setErrorMsg] = React.useState("");
    const [isChecked, setChecked] = React.useState(true);
    const contextData = React.useContext(AppContext);

    const { passwordVisibility, passwordRightIcon, handlePasswordVisibility } = useTogglePasswordVisibility();

    const login = async () => {
        const data = { email: email, password: password };
        setLoading(true);
        if(data.email != "" && data.password != "") {
            let response = await axios()
                .post("/login", data)
                .then(async (res) => {
                    contextData.setUserDataContext(res.data.user);
                    contextData.setUserAccessTokenContext(res.data.access_token);
                    let userDataString = JSON.stringify(res.data.user);
                    await AsyncStorage.setItem("user", userDataString);
                    await AsyncStorage.setItem("accessToken", res.data.access_token);
                    await AsyncStorage.setItem("accessedApp", 'true');
                    let userPointsDataString = JSON.stringify(res.data.user.points);
                    await AsyncStorage.setItem("user_points", userPointsDataString);
                    navigation.replace("BottomNavigator");
                })
                .catch((err) => {
                    setErrorMsg("Invalid Login Credentials");
                });
        } else {
            setErrorMsg("Email/Password is required.");
        }
        setLoading(false);

        if(isChecked) {
            await AsyncStorage.setItem("loginCredentials", JSON.stringify(data));
        } else {
            await AsyncStorage.removeItem("loginCredentials");
        }
    };

    React.useEffect(() => {
        fetchLoginCredentials();
    },[]);

    let fetchLoginCredentials = async () => {
        let loginCredentials = await AsyncStorage.getItem("loginCredentials");
        if(loginCredentials) {
            loginCredentials = JSON.parse(loginCredentials);
            setEmail(loginCredentials.email);
            setPassword(loginCredentials.password);
        }
    };

    let setCheckboxStatus = () => {
        setChecked(!isChecked);
    };

    const signUp = () => {
        navigation.navigate("RegistrationScreen");
    };

    let forgotPassword = () => {
        navigation.navigate("ForgotPasswordScreen");
    };

    if (loading) {
        return (
            <View style={styles.loadingContainer}>
                <ActivityIndicator size="large" color="#C9EA3B" />
                <Text style={styles.loadingText}>Loading...</Text>
            </View>
        );
    }

    return (
        <LinearGradient colors={['#ffffff', '#dedede', '#bababa']} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1.3 }} style={{ flex: 1 }}>
            <KeyboardAvoidingView style={styles.mainContainer} behavior={Platform.OS === "ios" ? "padding" : "height"} enabled>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <>
                        <View style={styles.card}>
                            <View style={styles.container}>
                                <Image
                                    style={styles.imgStyle}
                                    source={require("../../assets/logo.png")}
                                />
                            </View>
                            <View style={{ zIndex: 2 }}>
                                <TextInput
                                    style={styles.textInput}
                                    placeholder="Email"
                                    autoCapitalize="none"
                                    underlineColor="transparent"
                                    selectionColor="black"
                                    activeUnderlineColor="transparent"
                                    value={email}
                                    onChangeText={(text) => setEmail(text)}
                                />
                                <View style={[styles.fieldsContainer, styles.passwordInputContainer]}>
                                    <TextInput
                                        style={[styles.textInput, styles.marginBottom0, styles.width90percent]}
                                        placeholder="Password"
                                        autoCapitalize="none"
                                        underlineColor="transparent"
                                        selectionColor="black"
                                        activeUnderlineColor="transparent"
                                        secureTextEntry={passwordVisibility}
                                        value={password}
                                        onChangeText={(text) => setPassword(text)}
                                    />
                                    <View style={styles.passwordViewContainer}>
                                        <Pressable onPress={handlePasswordVisibility}>
                                            <MaterialCommunityIcons
                                                name={passwordRightIcon}
                                                size={22}
                                                color="#232323"
                                            />
                                        </Pressable>
                                    </View>
                                </View>
                            </View>
                            {error_msg != "" && (
                            <View style={styles.errorContainer}>
                                <Text style={styles.errorMessage}>{error_msg}</Text>
                            </View>
                            )}
                            <View style={styles.rememberForgotContainer}>
                                <TouchableOpacity style={[styles.checkboxContainer, styles.justifyContainer]} onPress={setCheckboxStatus}>
                                    <Checkbox style={styles.checkbox} value={isChecked} onValueChange={setChecked} color={isChecked ? '#C9EA3B' : undefined} />
                                    <Text style={styles.checkboxText}>Remember me</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.justifyContainer]} onPress={forgotPassword}>
                                    <Text style={[styles.forgotPassword]}>Forgot Password?</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.container}>
                                <TouchableOpacity
                                    style={styles.blackLimeOutlineBtn}
                                    onPress={login}
                                >
                                    <Text style={styles.limeText}>LOGIN</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.container}>
                                <TouchableOpacity onPress={signUp}>
                                    <Text style={styles.signUpText}>Don't have an account? <Text style={styles.signUpSubText}>Sign up</Text></Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </LinearGradient>
    );
};

export default LoginScreen;