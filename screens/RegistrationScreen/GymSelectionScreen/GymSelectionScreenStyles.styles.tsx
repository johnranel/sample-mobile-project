import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    gradientContainer: {
        flex: 1
    },

    mainContainer: {
        flex: 1, 
        width: "100%"
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },

    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    cardDescription: {
        color: "#676767", 
        marginBottom: 10
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    blackLimeOutlineBtn: {
        height: 40,
        width: (Dimensions.get('window').width - 25),
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#13181b",
        borderRadius: 25,
        borderColor: "#C9EA3B",
        borderWidth: 1.5,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 24,
    },

    headerMsg: {
        fontSize: 32,
        textAlign: "center",
        marginBottom: 20,
    },

    alignItemsCenter: {
        alignItems: "center"
    },

    marginTop0: {
        marginTop: 0
    },

    marginTop10: {
        marginTop: 10
    },

    marginBottom5: {
        marginBottom: 5
    },

    marginBottom10: {
        marginBottom: 10
    },

    marginBottom20: {
        marginBottom: 20
    },

    marginRight20: {
        marginRight: 20
    },

    marginLeft2: {
        marginLeft: 2
    },

    marginLeft5: {
        marginLeft: 5
    },

    marginVertical0: {
        marginVertical: 0
    },

    marginVertical10: {
        marginVertical: 10
    },

    padding5: {
        padding: 5
    },

    width50Percent: {
        width: "50%",
    },

    width80Percent: {
        width: "80%",
    },

    limeText: {
        color: "#C9EA3B",
        fontSize: 14,
    },

    limeBoldCenterText: {
        color: "#C9EA3B",
        fontSize: 14,
        fontWeight: "bold",
        textAlign: "center"
    },

    blackCenterText: {
        color: "black",
        fontSize: 14,
        textAlign: "center"
    },

    blackBoldText: {
        color: "black",
        fontSize: 14,
        fontWeight: "bold",
    },

    blackText: {
        color: "black",
        fontSize: 14
    },

    selectionButton: {
        height: 20, 
        width: "20%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#ccc",
    },

    modalContainer: {
        flex: 1, 
        alignItems: "center", 
        justifyContent: "center"
    },

    selectionContainer: {
        width: "60%", 
        alignItems: "flex-end"
    },

    selectionBlackButton: {
        height: 20, 
        alignItems: "center", 
        justifyContent: "flex-end", 
        borderRadius: 25, 
        flexDirection: "row", 
        backgroundColor: "black", 
        paddingHorizontal: 7,
        marginRight: 20
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    mapContainer: {
        borderRadius: 20, 
        marginBottom: 20,  
        shadowOffset: { width: 0, height: 0 }, 
        shadowColor: "#000", 
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1, 
        shadowRadius: 5, 
        elevation: 10, 
        overflow: "hidden"
    },

    map: {
        height: 200, 
        width: "100%"
    },

    gymFranchise: {
        flexDirection: "row", 
        marginBottom: 5, 
        alignItems: "center", 
        flexWrap: "wrap",
    },

    selectionPillsBtn: {
        height: 40, 
        width: "31.6%", 
        marginLeft: 5, 
        marginBottom: 5, 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    selectionTabsBtn: {
        flexDirection: "row", 
        padding: 10, 
        width: "100%", 
        marginBottom: 5, 
        borderRadius: 25, 
        borderWidth: 1.5
    },

    bgBlack: {
        backgroundColor: "black",
    },

    borderCustom: {
        borderWidth: 1.5, 
        borderColor: "#ccc"
    },
    
    bgBlackBorderLimeCustom: {
        borderWidth: 1.5, 
        borderColor: "#C9EA3B",
        backgroundColor: "#000"
    },

    textBold: {
        fontWeight: "bold"
    },

    errorMessage: {
        color: "red",
        fontSize: 14,
    },

    modalBtn: {
        height: 40, 
        width: "49%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    modalLimeBtn: {
        borderWidth: 1.5, 
        borderColor: "#C9EA3B",
        backgroundColor: "black",
        marginLeft: 5
    },

    modalBtnCstm: {
        borderWidth: 1.5, 
        borderColor: "#ccc", 
        marginLeft: 5
    },

    searchDropdown: {
        padding: 10,
        marginTop: 2,
        backgroundColor: '#ddd',
        borderColor: '#bbb',
        borderWidth: 1,
        borderRadius: 5,
    },

    maxHeight140: {
        maxHeight: 140
    },

    fontSize11: {
        fontSize: 11
    },

    paddingRight2: {
        paddingRight: 2
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    paddingHorizontal10: {
        paddingHorizontal: 10
    },

    paddingX2: {
        paddingLeft: 2,
        paddingRight: 2
    }

});