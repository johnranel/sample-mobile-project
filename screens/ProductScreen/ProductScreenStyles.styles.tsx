import { StyleSheet, Dimensions, Platform } from "react-native";
export default StyleSheet.create({
    gradientContainer: {
        flex: 1, 
        alignItems: "center"
    },

    mainContainer: {
        flexGrow: 1, 
        width: "100%"
    },

    card: {
        backgroundColor: "#ffffff", 
        marginTop: 20,
        width: (Dimensions.get('window').width - 25),
        borderRadius: 10,
        padding: 20,
        shadowOffset: { width: 0, height: 0 },
        shadowColor: "#000",
        shadowOpacity: (Platform.OS == "ios") ? 0.3 : 1,
        shadowRadius: 5,
        elevation: 10,
    },

    logo: {
        width: "100%", 
        resizeMode: "contain", 
        height: 120
    },

    cardNoBg: {
        width: (Dimensions.get('window').width - 25),
        marginTop: 20,
    },

    cardHeadline: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10,
    },

    selectionContainer: {
        flexDirection: "row", 
        marginBottom: 5, 
        alignItems: "center", 
        flexWrap: "wrap"
    },

    selectAllBtn: {
        height: 40, 
        width: "31.6%", 
        marginLeft: 5, 
        marginBottom: 5, 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25, 
        borderWidth: 1.5, 
        borderColor: "#ccc" 
    },

    categoryBtn: {
        height: 40, 
        width: "31.6%", 
        marginLeft: 5, 
        marginBottom: 5, 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25
    },

    borderGrayCustom: {
        borderWidth: 1.5, 
        borderColor: "#ccc"
    },

    borderLimeCustom: {
        borderWidth: 1.5, 
        borderColor: "#36b9cc"
    },

    bdrBgLimeCustom: {
        borderWidth: 1.5, 
        borderColor: "#36b9cc",
        backgroundColor: "black"
    },

    limeBoldCenterText: {
        color: "#36b9cc",
        fontSize: 14,
        fontWeight: "bold",
        textAlign: "center"
    },

    blackCenterText: {
        color: "black",
        fontSize: 14,
        textAlign: "center"
    },

    blackBoldText: {
        color: "black",
        fontSize: 14,
        fontWeight: "bold"
    },

    blackText: {
        color: "black",
        fontSize: 14,
    },

    whiteText: {
        color: "white",
        fontSize: 14,
    },

    tabContainer: {
        flexDirection: "row", 
        alignSelf: "flex-start", 
        marginHorizontal: 12.5, 
        marginTop: 20
    },

    categoryTabs: {
        flexDirection: "row", 
        height: 40, 
        width: "32.5%", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 25, 
        borderBottomLeftRadius: 0, 
        borderBottomRightRadius: 0
    },

    favoritesBtn: {
        flexDirection: "row", 
        paddingVertical: 2.5, 
        paddingHorizontal: 10, 
        borderWidth: 1.5, 
        borderColor: "#36b9cc", 
        borderRadius: 15, 
        alignContent: "center", 
        justifyContent: "center",
        width: 100
    },

    productContainer: {
        width: (Dimensions.get('window').width / 3) - 22, 
        marginBottom: 10, 
        padding: 5 
    },

    productInfo: {
        position: "absolute", 
        right: 15, 
        zIndex: 2, 
        paddingVertical: 5, 
        paddingHorizontal: 10, 
        backgroundColor: "#36b9cc", 
        borderRadius: 20, 
        alignItems: "center", 
        flexDirection: "row"
    },

    productImage: {
        height: 100,
        width: "100%",
        resizeMode: "contain"
    },

    productName: {
        textAlign: "center", 
        fontWeight: "bold", 
        color: "#676767", 
        marginTop: 5
    },

    textBold: {
        fontWeight: "bold"
    },

    bgWhite: {
        backgroundColor: "white",
    },

    bgBlack: {
        backgroundColor: "black",
    },

    alignItemsCenter: {
        alignItems: "center"
    },

    alignItemsFlexEnd: {
        alignItems: "flex-end"
    },

    alignSelfCenter: {
        alignSelf: "center"
    },

    alignSelfFlexStart: {
        alignSelf: "flex-start"
    },

    marginRight5: {
        marginRight: 5
    },

    marginBottom20: {
        marginBottom: 20
    },

    paddingHorizontal10: {
        paddingHorizontal: 10
    },

    width50Percent: {
        width: "50%",
    },

    fontSize11: {
        fontSize: 11
    },

    flxDrtnRow:{
        flexDirection: "row"
    },

    flxWrap: {
        flexWrap: "wrap"
    },

    displayFlex: {
        display: "flex"
    },

    displayNone: {
        display: "none"
    },

    textAlignRight: {
        textAlign: "right"
    },
    
    cardCustom: {
        borderTopLeftRadius: 0, 
        marginTop: 0
    },

    justifyContentSpaceBetween: {
        justifyContent: "space-between"
    },

    paddingX5: {
        paddingLeft: 5,
        paddingRight: 5
    },
});